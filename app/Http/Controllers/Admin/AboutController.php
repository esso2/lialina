<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Aboutus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Facades\DataTables;

class AboutController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:aboutus-read')->only(['index']);
        $this->middleware('permission:aboutus-create')->only(['create', 'store']);
        $this->middleware('permission:aboutus-update')->only(['edit', 'update']);
        $this->middleware('permission:aboutus-delete')->only(['destroy']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Aboutus::all();
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('image', function ($query) {
            return '<img src=' . $query->image_path . ' border="0" style=" width: 250px; height: 130px;" class="image-show" />';
        })
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('aboutus-update')){
             $btn ='<a href="' .route("aboutus.edit", $row->id). '" type="button" data-admid="'.$row->id.'" 
             class="btn btn-primary btn-xs edit"><i class="fa fa-pencil">
             </i></i></a> &nbsp;';
            }else{
                $btn = '<a  href="" class="btn btn-primary btn-xs disabled"><i class="fa fa-pencil"></i></i></a>';
            }
            if (Auth::guard('admin')->user()->hasPermission('aboutus-delete')){
                $btn = $btn.
                 '<form class=" delete"  action="' . route("aboutus.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-xs" title=" ' . 'Delete' . ' "><i class="fa fa-trash-o"></i></button>
                </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
                return $btn;
            })
                ->rawColumns(['action','image'])
                ->make(true);
            }

            return view('admin.aboutus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.aboutus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
       $request->validate([
        'ar.title' => 'required',
        'ar.description' => 'required',

        'en.title' => 'required',
        'en.description' => 'required',
    ]);

    $about = Aboutus::create([
        'ar' => [
            'title'       => $request->input('ar.title'),
            'description' => $request->input('ar.description')
        ],
        'en' => [
            'title'       => $request->input('en.title'),
            'description' => $request->input('en.description')
        ]
    ]);

    if ($request->image) {
        Image::make($request->image)->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('uploads/aboutus/' . $request->image->hashName()));

    }
    $about->image = $request->image->hashName();

    $about->save();

    return response()->json(['status'=>'success','data'=>$about]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = Aboutus::findOrFail($id);
        return view('admin.aboutus.edit',compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = Aboutus::findOrFail($id);
        $request->validate([
        'ar.title' => 'required',
        'ar.description' => 'required',
        
        'en.title' => 'required',
        'en.description' => 'required',
             ]);
        $about->update([
            'ar' => [
                'title'       => $request->input('ar.title'),
                'description' => $request->input('ar.description')
            ],
            'en' => [
                'title'       => $request->input('en.title'),
                'description' => $request->input('en.description')
            ]
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/users/' . $request->image->hashName()));
            $about->image = $request->image->hashName();
        }

        $about->save();
        return response()->json(['status'=>'success','data'=>$about]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = Aboutus::findOrFail($id);
        Storage::disk('public_uploads')->delete('/aboutus/' . $about->image);
        $about->delete();
        return response()->json(['status'=>'success']);
    }
}
