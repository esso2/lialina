<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:regions-read')->only(['index']);
        $this->middleware('permission:regions-create')->only(['create', 'store']);
        $this->middleware('permission:regions-update')->only(['edit', 'update']);
        $this->middleware('permission:regions-delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::all();
            return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('active', function ($query) {
                if ($query->active) {
                    $btn = '
            <div align="center">
            <label class="switch">
            <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
                <div class="slider round">
                    <span class="on">ON</span>
                    <span class="off">OFF</span>
                </div>
            </label>
          </div>';
                } else {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check">
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
              </div>';
                }

                return $btn;
            })
            ->addColumn('image', function ($query) {
            return '<img src=' . $query->image_path . ' border="0" style=" width: 250px; height: 130px;" class="image-show" />';
               })
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('categories-update')){
             $btn ='<a href="' .route("categories.edit", $row->id). '" type="button" data-admid="'.$row->id.'" 
             class="btn btn-primary btn-xs edit"><i class="fa fa-pencil">
             </i></i></a> &nbsp;';
            }else{
                $btn = '<a  href="" class="btn btn-primary btn-xs disabled"><i class="fa fa-pencil"></i></i></a>';
            }
            if (Auth::guard('admin')->user()->hasPermission('categories-delete')){
                $btn = $btn.
                 '<form class=" delete"  action="' . route("categories.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-xs" title=" ' . 'Delete' . ' "><i class="fa fa-trash-o"></i></button>
                </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
                return $btn;
            })
                ->rawColumns(['action','image','active'])
                ->make(true);
            }

            return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
       $request->validate([
        'ar.title' => 'required',
        'en.title' => 'required',
           ]);
  
            $category = Category::create([
                'ar' => [
                    'title'       => $request->input('ar.title'),
                ],
                'en' => [
                    'title'       => $request->input('en.title'),
                ]
            ]);

            if ($request->image) {
                Image::make($request->image)->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(public_path('uploads/categories/' . $request->image->hashName()));
            }
            $category->image = $request->image->hashName();

    $category->save();

    return response()->json(['status'=>'success','data'=>$category]);
    }


    public function CategoryStatus(Request $request)
    {
        $category = Category::find($request->category_id);
        $category->active = $request->active;
        $category->save();

        return response()->json(['status' => 'success', 'data' => $category]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $request->validate([
                'ar.title' => 'required',
                'en.title' => 'required',
             ]);
        $category->update([
            'ar' => [
                'title'       => $request->input('ar.title'),
            ],
            'en' => [
                'title'       => $request->input('en.title'),
            ]
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/categories/' . $request->image->hashName()));
            $category->image = $request->image->hashName();
        }

        $category->save();
        return response()->json(['status'=>'success','data'=>$category]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        Storage::disk('public_uploads')->delete('/categories/' . $category->image);
        $category->delete();
        return response()->json(['status'=>'success']);
    }
}
