<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Category;
use App\Models\Entity;
use App\Models\EntityDetails;
use App\Models\EntityFeature;
use App\Models\EntityFilterOption;
use App\Models\EntityImages;
use App\Models\Filter;
use App\Models\FilterOption;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Facades\DataTables;

class EntityController extends Controller
{


    public function __construct()
    {
        $this->middleware('permission:entities-read')->only(['index']);
        $this->middleware('permission:entities-create')->only(['create', 'store']);
        $this->middleware('permission:entities-update')->only(['edit', 'update']);
        $this->middleware('permission:entities-delete')->only(['destroy']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::guard('admin')->user()->id;
        $userRole = Auth::guard('admin')->user()->roles->first()->name;

        if ($request->ajax()) {
            if ($userRole == 'super_admin') {
                $data = Entity::all();
            }else{
                $data = Entity::where('admin_id',$user)->get();
            }
            
            return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('active', function ($query) {
                if ($query->active) {
                    $btn = '
            <div align="center">
            <label class="switch">
            <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
                <div class="slider round">
                    <span class="on">ON</span>
                    <span class="off">OFF</span>
                </div>
            </label>
          </div>';
                } else {
                    $btn = '
                <div align="center">
                <label class="switch">
                <input data-id="' . $query->id . '" type="checkbox" id="check">
                    <div class="slider round">
                        <span class="on">ON</span>
                        <span class="off">OFF</span>
                    </div>
                </label>
              </div>';
                }

                return $btn;
            })
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('entities-update')){
             $btn =' <a href="' .route("entities.edit", $row->id). '" type="button" data-admid="'.$row->id.'" 
             class="btn btn-primary btn-xs edit"><i class="fa fa-pencil">
             </i></i></a> &nbsp;';
            }else{
                $btn = ' <a  href="" class="btn btn-primary btn-xs disabled"><i class="fa fa-pencil"></i></i></a>';
            }
            if (Auth::guard('admin')->user()->hasPermission('entities-delete')){
                $btn = $btn.
                 ' <form class=" delete"  action="' . route("entities.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-xs" title=" ' . 'Delete' . ' "><i class="fa fa-trash-o"></i></button>
                </form>';
            }else{
                $btn = $btn. ' <button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }

            if (Auth::guard('admin')->user()->hasPermission('entities-read')){
                $btn =$btn. ' <a href="' .route("entities.show", $row->id). '" type="button" data-admid="'.$row->id.'" 
                class="btn btn-primary btn-xs edit"><i class="fa fa-eye">
                </i></i></a> &nbsp;';
               }else{
                   $btn =$btn. ' <a  href="" class="btn btn-primary btn-xs disabled"><i class="fa fa-eye"></i></i></a>';
               }

                return $btn;
            })
                ->rawColumns(['action','image','active'])
                ->make(true);
            }

        return view('admin.entities.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::all();
        $admins = Admin::whereHas('roles', function ($query) {
            $query->where('name', '!=', 'super_admin');
        })->where('active', 1)->get();
    
        $categories = Category::all();
        return view('admin.entities.create',compact('regions','admins','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //dd($request->all());
        $v = Validator::make($request->all(), [
        'ar.title' => 'required',
        'ar.description' => 'required',
        'ar.address' => 'required',

        'region_id' => 'required|not_in:0',
        'admin_id' => 'required|not_in:0',
        'cat_id' => 'required|not_in:0',

        'en.title' => 'required',
        'en.description' => 'required',
        'en.address' => 'required',
        ]);
        
        if ($v->fails()) {
            return Response::json(['errors' => $v->errors()]);
        }

        if (empty($request->input('family'))) {
            $family = 0;
        } else {
            $family = 1;
        }

        if (empty($request->input('showcode'))) {
            $showcode = 0;
        } else {
            $showcode = 1;
        }

        $entity = Entity::create([

            'region_id' => $request->region_id,
            'admin_id' => $request->admin_id,
            'cat_id' => $request->cat_id,
            'code' => $request->code,
            'space' => $request->space,
            'family' => $family,
            'show_code' => $showcode,

            'ar' => [
                'title'       => $request->input('ar.title'),
                'description' => $request->input('ar.description'),
                'address'     => $request->input('ar.address')
            ],
            'en' => [
                'title'       => $request->input('en.title'),
                'description' => $request->input('en.description'),
                'address'     => $request->input('en.address')
            ],
        ]);

        $entity->save();

        session()->flash('success', __('admin.addsuccessfully'));
        return redirect()->route('entities.edit',$entity->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Entity::findOrFail($id);
        $regions = Region::all();
        $admins = Admin::whereHas('roles', function ($query) {
            $query->where('name', '!=', 'super_admin');
        })->where('active', 1)->get();
    
        $categories = Category::all();
        $types = ['sets','pools','wc','bedrooms','kitchen','amenities','condition'];
        $filters = Filter::all();
        return view('admin.entities.edit',compact('entity','regions','admins','categories','types','filters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entity = Entity::findOrFail($id);

        $v = Validator::make($request->all(), [
            'ar.title' => 'required',
            'ar.description' => 'required',
            'ar.address' => 'required',
    
            'region_id' => 'required|not_in:0',
            'admin_id' => 'required|not_in:0',
            'cat_id' => 'required|not_in:0',
    
            'en.title' => 'required',
            'en.description' => 'required',
            'en.address' => 'required',
            ]);
            
            if ($v->fails()) {
                return Response::json(['errors' => $v->errors()]);
            }

             if (empty($request->input('family'))) {
                $family = 0;
            } else {
                $family = 1;
            }
    
            if (empty($request->input('showcode'))) {
                $showcode = 0;
            } else {
                $showcode = 1;
            }

             $entity->update([
                'region_id' => $request->region_id,
                'admin_id' => $request->admin_id,
                'cat_id' => $request->cat_id,
                'code' => $request->code,
                'space' => $request->space,
                'family' => $family,
                'show_code' => $showcode,
    
                'ar' => [
                    'title'       => $request->input('ar.title'),
                    'description' => $request->input('ar.description'),
                    'address'     => $request->input('ar.address')
                ],
                'en' => [
                    'title'       => $request->input('en.title'),
                    'description' => $request->input('en.description'),
                    'address'     => $request->input('en.address')
                ],
            ]);

            return response()->json(['status'=>'success','data'=>$entity]);
    }

    public function EntityStatus(Request $request)
    {
        $entity = Category::find($request->entity_id);
        $entity->active = $request->active;
        $entity->save();

        return response()->json(['status' => 'success', 'data' => $entity]);
    }

    public function updateStep2(Request $request, $id)
    {
        // dd($request->all());
         $v = Validator::make($request->all(), [
            'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'price_weekend' => 'required|regex:/^\d+(\.\d{1,2})?$/', 
            'deposit' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'mobile' => 'required|numeric', 
            'facebook' => ['required','regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],  
            'whatsapp' => 'required|numeric', 
            'instgram' => ['required','regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],  
            'twitter' => ['required','regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'], 
            'lat' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'], 
            'lang' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],  
            'check_in_am' => 'required', 
            'check_out_am' => 'required', 
            'check_in_pm' => 'required', 
            'check_out_pm' => 'required'
         ]
        );

        if ($v->fails()) {
            return Response::json(['errors' => $v->errors()]);
        }

        if(EntityDetails::where('entity_id', '=', $request->EntityId)->exists()){ 
          $entitydetails = EntityDetails::findOrFail($id);
          $entitydetails->update([
            'price' => $request->price,
            'price_weekend' => $request->price_weekend,
            'deposit' => $request->deposit,
            'mobile' => $request->mobile,
            'facebook' => $request->facebook,
            'whatsapp' => $request->whatsapp,
            'instgram' => $request->instgram,
            'twitter' => $request->twitter,
            'lat' => $request->lat,
            'lang' => $request->lang,
            'check_in_am' => $request->check_in_am,
            'check_out_am' => $request->check_out_am,
            'check_in_pm' => $request->check_in_pm,
            'check_out_pm' => $request->check_out_pm,
          ]);
        }else{
          $entitydetails = EntityDetails::create([
            'entity_id' => $request->EntityId,
            'price' => $request->price,
            'price_weekend' => $request->price_weekend,
            'deposit' => $request->deposit,
            'mobile' => $request->mobile,
            'facebook' => $request->facebook,
            'whatsapp' => $request->whatsapp,
            'instgram' => $request->instgram,
            'twitter' => $request->twitter,
            'lat' => $request->lat,
            'lang' => $request->lang,
            'check_in_am' => $request->check_in_am,
            'check_out_am' => $request->check_out_am,
            'check_in_pm' => $request->check_in_pm,
            'check_out_pm' => $request->check_out_pm,
            ]);
        }
            return response()->json(['status'=>'success','data'=>$entitydetails]);
    }


    public function EntityImage(Request $request)
    {
        $entitydetails = EntityImages::create([
            'entity_id' => $request->EntityId,
        ]);

        $image = $request->file('file');
        if ($image) {
            Image::make($image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/entities/' . $image->hashName()));
    
        }
        $entitydetails->image = $image->hashName();
    
        $entitydetails->save();

        return response()->json(['status'=>'success','data'=>$entitydetails]);
    }


    public function ShowImage(Request $request)
    {
        $myitemfiles = EntityImages::where('entity_id', '=', $request->entity_id)->get();
        foreach($myitemfiles as $file){ //get an array which has the names of all the files and loop through it 
            $obj['name'] = $file; //get the filename in array
            $obj['size'] = filesize(public_path('uploads/entities/').$file->image); //get the flesize in array
            $result[] = $obj; // copy it to another array
          }

        return json_encode($result);
    }

    public function EntityImageDelete(Request $request)
    {
            $name =  $request->get('name');
            $photo = EntityImages::where('image',$name)->first();
            Storage::disk('public_uploads')->delete('/entities/' . $photo->image);
            $photo->delete();
    
            return response()->json(['status'=>'success','data'=>$photo]);
    
    }


    public function EntityFeatureAdd(Request $request)
    {
         //dd($request->all());
         $v = Validator::make($request->all(), [
            'ar.title' => 'required',
            'type_id' => 'required|not_in:0',
            'en.title' => 'required',
            ]);
            
            if ($v->fails()) {
                return Response::json(['errors' => $v->errors()]);
            }

            $feature = EntityFeature::create([
                'entity_id' => $request->EntityId,
                'type' => $request->type_id,
                'ar' => [
                    'title'       => $request->input('ar.title'),
                 ],
                'en' => [
                    'title'       => $request->input('en.title'),
                 ],
            ]);
    
            $feature->save();
            return response()->json(['status'=>'success','data'=>$feature]);
    }


    public function EntityFeatureAll(Request $request)
    {
        $id = $request->input('id');
        if ($request->ajax()) {
            $data = EntityFeature::where('entity_id',$id)->get();
            return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('type', function ($query) {
                $types = ['sets','pools','wc','bedrooms','kitchen','amenities','condition'];
                foreach ($types as $key => $type) {
                    if ($query->type == $key+1 ) {
                        return __('admin.'.$type);
                    }
                }
                
             })
            ->addColumn('action', function($row){
            
            if (Auth::guard('admin')->user()->hasPermission('entities-delete')){
                $btn = 
                 '<form class=" delete"  action="' . route("featureDelete", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-xs" title=" ' . 'Delete' . ' "><i class="fa fa-trash-o"></i></button>
                </form>';
            }else{
                $btn ='<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
                return $btn;
            })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function EntityFeatureDelete($id)
    {
        $feat = EntityFeature::findOrFail($id);
        $feat->delete();
        return response()->json(['status'=>'success']);
    }


    public function EntityFilterDelete($id)
    {
        $filter = Filter::findOrFail($id);
        $filter->delete();
        return response()->json(['status'=>'success']);
    }


    public function EntityOptionDelete($id)
    {
        $option = FilterOption::findOrFail($id);
        $option->delete();
        return response()->json(['status'=>'success']);
    }


    public function EntityFilterAdd(Request $request)
    {
        // dd($request->all());
         $v = Validator::make($request->all(), [
            'ar.title' => 'required',
            'en.title' => 'required',
            ]);
            
            if ($v->fails()) {
                return Response::json(['errors' => $v->errors()]);
            }

            $filter = Filter::create([
                'ar' => [
                    'title' => $request->input('ar.title'),
                 ],
                'en' => [
                    'title' => $request->input('en.title'),
                 ],
            ]);
            $filter->save();

            foreach($request->input('ar.option_title') as $key => $value) {
                $filteroption = FilterOption::create([
                    'filter_id' => $filter->id,
                    'ar' => [
                    'title' => $value,
                     ],
                     'en' => [
                    'title' => $request->input('en.option_title')[$key],
                     ],
                ]);
            }
    
            $filteroption->save();

            return response()->json(['status'=>'success','data'=>$filteroption]);
    }


    public function EntityOptionAdd(Request $request)
    {
        $entity = EntityFilterOption::create([
            'entity_id' => $request->entity_id,
            'filter_option_id' => $request->option_id,
        ]);
        $entity->save();
        return response()->json(['status'=>'success','data'=>$entity]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entity = Entity::findOrFail($id);
        foreach ($entity->images as $value) {
            Storage::disk('public_uploads')->delete('/entities/' . $value->image);
        }
        $entity->delete();
        return response()->json(['status'=>'success']);
    }
}
