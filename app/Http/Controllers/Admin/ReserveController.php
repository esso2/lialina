<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Entity;
use App\Models\Reserve;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ReserveController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:reserves-read')->only(['index']);
        $this->middleware('permission:reserves-create')->only(['create', 'store']);
        $this->middleware('permission:reserves-update')->only(['edit', 'update']);
        $this->middleware('permission:reserves-delete')->only(['destroy']);
    }


    public function index()
    {
        
         $user = Auth::guard('admin')->user()->id;
         $userRole = Auth::guard('admin')->user()->roles->first()->name;
         $AdminEntitis = Auth::guard('admin')->user()->entities;
         
        if ($userRole == 'super_admin') {
            $events = Reserve::all();
            $users = User::all();
            $entities = Entity::all();

         }else{
        foreach ($AdminEntitis as $value) {
            $events = Reserve::where('entity_id',$value->id)->get();
            }

            return view('admin.reserves.index',compact('events','admins','users','entities'));
         }
        return view('admin.reserves.index',compact('events','users','entities'));
    }

    public function GetAllReseve(Request $request)
    {

        if ($request->ajax()) {
            $data = Reserve::where('from',$request->start_date)
            ->where('to',$request->end_date)
            ->get();
            return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('from', function ($query) {
                $from = Carbon::parse($query->from)->format('Y-m-d');
                return $from;
            })
            ->editColumn('to', function ($query) {
                $from = Carbon::parse($query->to)->format('Y-m-d');
                return $from;
            })
            ->editColumn('title', function ($query) {
                $title = $query->entity->title;
                return $title;
            })
            ->editColumn('price', function ($query) {
                $price = $query->entity->detailes->price;
                return $price;
            })
            ->editColumn('user', function ($query) {
                $name = $query->user->name;
                return $name;
            })
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('reserves-update')){
             $btn ='<a href="' .route("reserves.edit", $row->id). '" type="button" data-admid="'.$row->id.'" 
             class="btn btn-primary btn-xs edit"><i class="fa fa-pencil">
             </i></i></a> &nbsp;';
            }else{
                $btn = '<a  href="" class="btn btn-primary btn-xs disabled"><i class="fa fa-pencil"></i></i></a>';
            }
            if (Auth::guard('admin')->user()->hasPermission('reserves-delete')){
                $btn = $btn.
                 '<form class=" delete"  action="' . route("reserves.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-xs" title=" ' . 'Delete' . ' "><i class="fa fa-trash-o"></i></button>
                </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
                return $btn;
            })
                ->rawColumns(['action'])
                ->make(true);
            }

            return view('admin.aboutus.index');
    }

    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    $request->validate([
        'start_date' => 'required',
        'end_date' => 'required',
        'entity_id' => 'required',
        'client_id' => 'required',
    ]);

    $entity = Entity::findOrFail($request->entity_id);
    $admin = $entity->admin->id;
    $price = $entity->detailes->price;
    $resrve = Reserve::create([
        'admin_id' => $admin,
        'entity_id' => $request->entity_id,
        'user_id' => $request->client_id,
        'from' => $request->start_date,
        'to' => $request->end_date,
        'paid' => $request->paid,
        'remain' => ($price - $request->paid),
        'notes' => $request->notes,
    ]);

    $resrve->save();
    return response()->json(['status'=>'success','data'=>$resrve]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resrve = Reserve::findOrFail($id);
        $entity = Entity::findOrFail($resrve->entity_id);
        return view('admin.reserves.edit',compact('resrve','entity'));
    }

    /*
     *
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resrve = Reserve::findOrFail($id);
        $resrve->delete();
        return response()->json(['status'=>'success']);
    }
}
