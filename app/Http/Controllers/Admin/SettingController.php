<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;


class SettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:settings-read')->only(['index']);
        $this->middleware('permission:settings-update')->only(['update']);
    }

    public function index()
    {
        $setting = Setting::first();
        return view('admin.settings.index', compact('setting'));
    }


    public function update(Request $request)
    {
        $setting = Setting::findOrFail(1);
        $request->validate([
            'title'      => 'required | min:3',
            'logo'    => 'required',
            'facebook' => 'required',
            'instagram'   => 'required',
            'twitter'   => 'required',
            'phone'   => 'required',
            'whatsapp'   => 'required',
            'website'   => 'required',
            'email'   => 'required',
            'address'   => 'required',
        ]);
        $setting->update([
            'title' => $request->title,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'twitter' => $request->twitter,
            'phone' => $request->phone,
            'whatsapp' => $request->whatsapp,
            'website' => $request->website,
            'email' => $request->email,
            'address' => $request->address,
        ]);

        if ($request->logo) {
            Image::make($request->logo)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/website/' . $request->logo->hashName()));
        }
        $setting->logo = $request->logo->hashName();

        $setting->save();
        return response()->json(['status'=>'success','data'=>$setting]);
        
    }

    
}
