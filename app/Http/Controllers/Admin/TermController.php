<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Terms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class TermController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:terms-read')->only(['index']);
        $this->middleware('permission:terms-create')->only(['create', 'store']);
        $this->middleware('permission:terms-update')->only(['edit', 'update']);
        $this->middleware('permission:terms-delete')->only(['destroy']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Terms::all();
            return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('description', function ($query) {
                return Str::limit($query->description, 100);
            })
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('terms-update')){
             $btn ='<a href="' .route("terms.edit", $row->id). '" type="button" data-admid="'.$row->id.'" 
             class="btn btn-primary btn-xs edit"><i class="fa fa-pencil">
             </i></i></a> &nbsp;';
            }else{
                $btn = '<a  href="" class="btn btn-primary btn-xs disabled"><i class="fa fa-pencil"></i></i></a>';
            }
            if (Auth::guard('admin')->user()->hasPermission('terms-delete')){
                $btn = $btn.
                 '<form class=" delete"  action="' . route("terms.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-xs" title=" ' . 'Delete' . ' "><i class="fa fa-trash-o"></i></button>
                </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
                return $btn;
            })
                ->rawColumns(['action'])
                ->make(true);
            }

            return view('admin.terms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.terms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $request->validate([
        'ar.title' => 'required',
        'ar.description' => 'required',

        'en.title' => 'required',
        'en.description' => 'required',
    ]);

    $term = Terms::create([
        'ar' => [
            'title'       => $request->input('ar.title'),
            'description' => $request->input('ar.description')
        ],
        'en' => [
            'title'       => $request->input('en.title'),
            'description' => $request->input('en.description')
        ]
    ]);

      $term->save();

    return response()->json(['status'=>'success','data'=>$term]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $term = Terms::findOrFail($id);
        return view('admin.terms.edit',compact('term'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $term = Terms::findOrFail($id);
        $request->validate([
        'ar.title' => 'required',
        'ar.description' => 'required',
        
        'en.title' => 'required',
        'en.description' => 'required',
             ]);
        $term->update([
            'ar' => [
                'title'       => $request->input('ar.title'),
                'description' => $request->input('ar.description')
            ],
            'en' => [
                'title'       => $request->input('en.title'),
                'description' => $request->input('en.description')
            ]
        ]);
        $term->save();
        return response()->json(['status'=>'success','data'=>$term]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $term = Terms::findOrFail($id);
        $term->delete();
        return response()->json(['status'=>'success']);
    }
}
