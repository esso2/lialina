<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::all();
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('image', function ($query) {
            return '<img src=' . $query->image_path . ' border="0" style=" width: 80px; height: 80px;" class="image-show img-circle" />';
        })
        ->editColumn('active', function ($query) {
            if ($query->active) {
                $btn = '
        <div align="center">
        <label class="switch">
        <input data-id="' . $query->id . '" type="checkbox" id="check" checked>
            <div class="slider round">
                <span class="on">ON</span>
                <span class="off">OFF</span>
            </div>
        </label>
      </div>';
            } else {
                $btn = '
            <div align="center">
            <label class="switch">
            <input data-id="' . $query->id . '" type="checkbox" id="check">
                <div class="slider round">
                    <span class="on">ON</span>
                    <span class="off">OFF</span>
                </div>
            </label>
          </div>';
            }

            return $btn;
        })
            ->addColumn('action', function($row){
            if (Auth::guard('admin')->user()->hasPermission('users-update')){
             $btn ='<a href="' .route("users.edit", $row->id). '" type="button" data-admid="'.$row->id.'" 
             class="btn btn-primary btn-xs edit"><i class="fa fa-pencil">
             </i></i></a> &nbsp;';
            }else{
                $btn = '<a  href="" class="btn btn-primary btn-xs disabled"><i class="fa fa-pencil"></i></i></a>';
            }
            if (Auth::guard('admin')->user()->hasPermission('users-delete')){
                $btn = $btn.
                '<form class=" delete"  action="' . route("users.destroy", $row->id) . '"  method="POST" id="delform"
                style="display: inline-block; right: 50px;" >
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <button type="submit" class="btn btn-danger btn-xs" title=" ' . 'Delete' . ' "><i class="fa fa-trash-o"></i></button>
                </form>';
            }else{
                $btn = $btn. '<button class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></button>';
            }
                return $btn;
            })
                ->rawColumns(['action','image','active'])
                ->make(true);
            }

            return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      //  dd($request->all());
        $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
        ]);

        $user = User::create([
            'name' => $request->name,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'active' => 1,
            'password' => Hash::make($request->password),
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/users/' . $request->image->hashName()));

            $user->image = $request->image->hashName();
        }
        $user->save();
        return response()->json(['status'=>'success','data'=>$user]);
    }



    public function UserStatus(Request $request)
    {
        $user = User::find($request->user_id);
        $user->active = $request->active;
        $user->save();
        return response()->json(['status' => 'success', 'data' => $user]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'email' => ['required', Rule::unique('admins')->ignore($user->id),],
            //'password' => 'required',
        ]);
        $user->update([
            'name' => $request->name,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        if ($request->image) {
            Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/users/' . $request->image->hashName()));
            $user->image = $request->image->hashName();
        }
        $user->save();
        return response()->json(['status'=>'success','data'=>$user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user->image != 'default.png') {
        Storage::disk('public_uploads')->delete('/users/' . $user->image);
        }
        
        $user->delete();
        return response()->json(['status'=>'success']);
    }
}
