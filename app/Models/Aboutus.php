<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Aboutus extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'aboutuses';

    public $translatedAttributes = ['title', 'description'];

    protected $fillable = [
        'image',
        'active',
    ];

    protected $appends = ['image_path'];

    
    public function getImagePathAttribute()
    {
        return asset('uploads/aboutus/' . $this->image);
    }
}
