<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Category extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'categories';

    public $translatedAttributes = ['title'];

    protected $fillable = [
        'image',
        'active',
    ];

    protected $appends = ['image_path'];
   
    public function getImagePathAttribute()
    {
        return asset('uploads/categories/' . $this->image);
    }
    
    public function entities()
    {
        return $this->hasMany(Entity::class);
    }

}
