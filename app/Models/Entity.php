<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Psy\Input\FilterOptions;

class Entity extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'entities';

    public $translatedAttributes = ['title','description','address'];

    protected $fillable = [
        'cat_id',
        'region_id',
        'admin_id',
        'active',
        'family',
        'code',
        'show_code',
        'space',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function detailes(){
        return $this->hasOne(EntityDetails::class);
    }

    public function features(){
        return $this->hasMany(EntityFeature::class);
    }

    public function images(){
        return $this->hasMany(EntityImages::class);
    }

    public function filteroptions()
    {
        return $this->belongsToMany(FilterOption::class,'entity_filter_options','entity_id','filter_option_id');
    }

    public function resrves(){
        return $this->hasMany(Reserve::class);
    }

}
