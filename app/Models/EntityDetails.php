<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntityDetails extends Model
{
    use HasFactory;

    protected $table = 'entity_details';

    protected $fillable = [
        'entity_id',
        'price', 
        'price_weekend', 
        'deposit',
        'mobile', 
        'facebook', 
        'whatsapp', 
        'instgram', 
        'twitter', 
        'lat', 
        'lang', 
        'check_in_am', 
        'check_out_am', 
        'check_in_pm', 
        'check_out_pm'
    ];

    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

}
