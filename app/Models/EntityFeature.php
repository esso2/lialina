<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class EntityFeature extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'entity_features';

    public $translatedAttributes = ['title'];

    protected $fillable = [
        'entity_id',
        'type',
        'active',
    ];

    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

}
