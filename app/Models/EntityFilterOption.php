<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntityFilterOption extends Model
{
    use HasFactory;

    protected $table = 'entity_filter_options';

    protected $fillable = [
        'entity_id',
        'filter_option_id',
    ];


}
