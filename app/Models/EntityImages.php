<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntityImages extends Model
{
    use HasFactory;

    protected $table = 'entity_images';

    protected $fillable = [
        'entity_id',
        'image',
    ];

    protected $appends = ['image_path'];
   
     public function getImagePathAttribute()
    {
     return asset('uploads/entities/' . $this->image);
    }

    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }
    
}
