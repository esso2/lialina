<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Filter extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'filters';

    public $translatedAttributes = ['title'];

    protected $fillable = [
        'active',
    ];

    public function filteroptions(){
        return $this->hasMany(FilterOption::class);
    }

}
