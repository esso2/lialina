<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use FFI;

class FilterOption extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'filter_options';

    public $translatedAttributes = ['title'];

    protected $fillable = [
        'filter_id',
    ];

    public function entities()
    {
        return $this->belongsToMany(Entity::class,'entity_filter_options','entity_id','filter_option_id');
    }

    public function filters()
    {
        return $this->belongsTo(Filter::class);
    }
}
