<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Region extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'regions';

    public $translatedAttributes = ['title'];

    protected $fillable = [
        'active',
    ];

    public function entities()
    {
        return $this->hasMany(Entity::class);
    }
    
}
