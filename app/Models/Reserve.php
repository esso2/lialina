<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    use HasFactory;

    protected $table = 'reserves';

    protected $fillable = [
        'entity_id',
        'user_id',
        'admin_id',
        'from',
        'to',
        'created',
        'confirm',
        'paid',
        'remain',
        'notes',
    ];

    public function entity(){
        return $this->belongsTo(Entity::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    
}
