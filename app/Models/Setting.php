<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $table = 'settings';

    protected $fillable = [
        'title',
        'logo',
        'facebook',
        'instagram',
        'twitter',
        'linkedin',
        'phone',
        'whatsapp',
        'website',
        'email',
        'address',
    ];

    protected $appends = ['image_path'];

    
    public function getImagePathAttribute()
    {
        return asset('uploads/website/' . $this->logo);
    }

}
