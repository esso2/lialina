<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Terms extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'terms';
    public $translatedAttributes = ['title', 'description'];

}
