<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [

        'super_admin' => [
            'admins' => 'c,r,u,d',
            'aboutus' => 'c,r,u,d',
            'terms' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
            'roles' => 'c,r,u,d',
            'users' => 'c,r,u,d',
            'regions' => 'c,r,u,d',
            'entities' => 'c,r,u,d',
            'reserves' => 'c,r,u,d',
            'settings' => 'r,u',
            'contactus' => 'r,d',
           
        ],

        // 'user' => [
        //     'profile' => 'r,u',
        // ],

        // 'role_name' => [
        //     'module_1_name' => 'c,r,u,d',
        // ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
