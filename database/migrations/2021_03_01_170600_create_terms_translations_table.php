<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTermsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('terms_id');
            $table->string('locale')->index();
            $table->string('title');
            $table->text('description');

            $table->unique(['terms_id', 'locale']);
            $table->foreign('terms_id')->references('id')->on('terms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms_translations');
    }
}
