<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntityDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('entity_id');
            $table->integer('price')->default(0);
            $table->integer('price_weekend')->default(0);
            $table->integer('deposit')->default(0);
            $table->string('mobile')->default('000-00-000');
            $table->string('facebook')->default('facebook');
            $table->string('whatsapp')->default('whatsapp');
            $table->string('instgram')->default('instgram');
            $table->string('twitter')->default('twitter');
            $table->string('lat')->default('00.0000000');
            $table->string('lang')->default('00.0000000');
            $table->time('check_in_am')->nullable();
            $table->time('check_out_am')->nullable();
            $table->time('check_in_pm')->nullable();
            $table->time('check_out_pm')->nullable();
            $table->foreign('entity_id')->references('id')->on('entities')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_details');
    }
}
