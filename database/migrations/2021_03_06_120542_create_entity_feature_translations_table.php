<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntityFeatureTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_feature_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('entity_feature_id');
            $table->string('locale')->index();

            $table->string('title');

            $table->unique(['entity_feature_id', 'locale']);
            $table->foreign('entity_feature_id')->references('id')->on('entity_features')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_feature_translations');
    }
}
