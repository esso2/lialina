@extends('admin.layout.master')

@section('content')

<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.aboutus')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.aboutus')</a></li>
                            <li class="active">@lang('admin.editaboutus')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <form id="aboutus" method="post" enctype="multipart/form-data" data-parsley-validate>
                                @csrf
                                @method('put')
                                <input type="hidden" name="AboutusId" id="aboutus_id" value="{{$about->id}}"  class="form-control">
                              
                                @foreach (config('translatable.locales') as $locale)
                                <div class="form-group col-md-6">
                                    <label for="title">@lang('admin.' .$locale. '.title')</label>
                                    <input type="text" name="{{$locale}}[title]"
                                        value="{{$about->translate($locale)->title}}" 
                                        required parsley-trigger="change" 
                                        placeholder="@lang('admin.'.$locale .'.title')" 
                                        class="form-control" >
                                </div>
                            @endforeach
                                
                
                                @foreach (config('translatable.locales') as $locale)
                                <div class="form-group col-md-6">
                                    <label for="description">@lang('admin.' .$locale. '.description')</label>
                                    <textarea type="text" name="{{$locale}}[description]"
                                        value="" 
                                        required parsley-trigger="change" 
                                        placeholder="@lang('admin.'.$locale .'.description')" 
                                        class="form-control" >{{$about->translate($locale)->description}}</textarea>
                                </div>
                                @endforeach
                
                
                                <div class="form-group col-md-6" style="text-align: center;margin-top: 17px;">
                                    <label for="image"> @lang('admin.image')</label>
                                    <input type="file" name="image" id="image" class="filestyle image" data-buttonname="btn-primary">
                                </div>
                
                                <div class="form-group col-md-6">
                                    <img src="{{$about->image_path}}" 
                                    width="250" height="130" alt="" 
                                    style="margin-right: 50px;" 
                                    class="image-show"/>
                                 </div>
                               
                

                
                                <div class="clearfix"></div>
                                <div class="form-group text-right m-b-0">
                                    @if(Auth::guard('admin')->user()->hasPermission('aboutus-update'))
                                    <button class="btn btn-primary waves-effect waves-light" type="submit" name="submit">@lang('admin.edit')</button>
                                    @else
                                    <button class="btn btn-primary waves-effect waves-light" disabled>@lang('admin.edit')</button>
                                    @endif
                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">@lang('admin.cancel')</button>
                                </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#aboutus',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                 let id = $('#aboutus_id').val();
                 var url = '{{ route('aboutus.update', ':id') }}';
                  url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if (response.errors){
                            $.each(response.errors, function( index, value ) {

                                // console.log(value);

                                new Noty({
                                    type: 'success',
                                    layout: 'topRight',
                                    text: value,
                                    timeout: 2000,
                                    killer: true
                                }).show();
                            });
                        }
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.updatesuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },

                });
            });

        });
 </script>
@endpush