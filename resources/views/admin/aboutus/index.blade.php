@extends('admin.layout.master')

@section('content')


<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <h4 class="page-title">@lang('admin.aboutus')</h4>
                            <ol class="breadcrumb">
                                <li><a href="">@lang('admin.aboutus')</a></li>
                                <li class="active">@lang('admin.aboutus')</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <table class="table table-bordered role-table">
                                <thead>
                                <tr>
                                  <th class="text-center">#</th>
                                  <th class="text-center">@lang('admin.title')</th>
                                  <th class="text-center">@lang('admin.image')</th>
                                  <th class="text-center"></th>
                                </tr>
                                </thead>
                              </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
 $(function () {
    var locale = '{{ config('app.locale') }}';
    //console.log(locale);
    if (locale == 'ar') {
        var table = $('.role-table').DataTable({
       "language": {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Arabic.json"},
       processing: true,
       serverSide: true,
       ajax: "{{ route('aboutus.index') }}",
       
       columns: [
           {data: 'id', name: 'id'},
           {data: 'title', name: 'title',searchable: true, sortable : true},
           {data: 'image', name: 'image', sortable : true},
           {data: 'action', name: 'action', orderable: false, searchable: false},
       ],

       responsive:true,
       order:[0,'desc']
   });      
        } else {
            var table = $('.role-table').DataTable({
       
       "language": {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json"},
       processing: true,
       serverSide: true,
       ajax: "{{ route('aboutus.index') }}",
       
       columns: [
           {data: 'id', name: 'id'},
           {data: 'title', name: 'title',searchable: true, sortable : true},
           {data: 'image', name: 'image', sortable : true},
           {data: 'action', name: 'action', orderable: false, searchable: false},
       ],

       responsive:true,
       order:[0,'desc']
   }); 

}
        

    $('body').on('submit','#delform',function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        $.ajax({
          url: url,
          method: "delete",
          data: {
              _token: '{{ csrf_token() }}',
          },
          success: function (response) {

              if (response.status == 'success'){
                  new Noty({
                      type: 'success',
                      layout: 'topRight',
                      text: "@lang('admin.deletesuccessfully')",
                      timeout: 5000,
                      killer: true
                  }).show();
                  table.ajax.reload();
              }
              // console.log(response);
              
          }
        });
        })
});    
</script>

@endpush