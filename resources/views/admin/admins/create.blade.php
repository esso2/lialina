@extends('admin.layout.master')

@push('css')
<style>
.field-icon {
  float: left;
  margin-left: 15px;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}
</style>
@endpush
@section('content')

<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.admins')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.admins')</a></li>
                            <li class="active">@lang('admin.addadmins')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                <form id="admins" method="post" data-parsley-validate>
                @csrf
                @method('post')

                <div class="form-group col-md-6">
                    <label for="name">@lang('admin.name')</label>
                    <input type="text" name="name"
                        value="{{old('name')}}" 
                        required parsley-trigger="change" 
                        placeholder="@lang('admin.name')" 
                        class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="email">@lang('admin.email')</label>
                    <input type="email" name="email"
                        value="{{old('email')}}" 
                        required parsley-trigger="change" 
                        placeholder="@lang('admin.email')" 
                        class="form-control" >
                </div>

                <div class="form-group col-md-6" >
                    <label for="password">@lang('admin.password')</label>
                    <input type="password" name="password"
                        value="{{old('password')}}" 
                        required parsley-trigger="change" 
                        placeholder="@lang('admin.password')" 
                        class="form-control" id="password-field">
                        <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                </div>
                
                <div class="form-group col-md-6">
                    <label for="role_id"> @lang('admin.roles')</label>
                    <select class="form-control select2" name="role_id" required parsley-trigger="change" >
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="clearfix"></div>
                <div class="form-group text-right m-b-0">
                    @if(Auth::guard('admin')->user()->hasPermission('admins-create'))
                    <button class="btn btn-primary waves-effect waves-light" type="submit" name="submit">@lang('admin.add')</button>
                    @else
                    <button class="btn btn-primary waves-effect waves-light" disabled>@lang('admin.add')</button>
                    @endif
                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">@lang('admin.cancel')</button>
                </div>
                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

<script>
    $(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
            input.attr("type", "text");
            } else {
            input.attr("type", "password");
            }
    });
</script>
<script>
    $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#admins',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                $.ajax({
                    url: '{{ route('admins.store')}}',
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.addsuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },

                });
            });

        });
 </script>
@endpush