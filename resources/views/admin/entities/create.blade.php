@extends('admin.layout.master')

@push('css')

@endpush
@section('content')

<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.entities')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.entities')</a></li>
                            <li class="active">@lang('admin.addentities')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="container">
                                <div class="row">
                                    <section>
                                        <div class="wizard">
                                            <div class="wizard-inner">
                                                <div class="connecting-line"></div>
                                                <ul class="nav nav-tabs" role="tablist">

                                                    <li role="presentation" class="active">
                                                        <a href="#step1" data-toggle="tab" aria-controls="step1"
                                                            role="tab" title="@lang('admin.maininfo')">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-home"></i>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li role="presentation" class="disabled">
                                                        <a href="#step2" data-toggle="tab" aria-controls="step2"
                                                            role="tab" title="@lang('admin.details')">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-tasks"></i>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li role="presentation" class="disabled">
                                                        <a href="#step3" data-toggle="tab" aria-controls="step3"
                                                            role="tab" title="@lang('admin.images')">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-picture"></i>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="disabled">
                                                        <a href="#step4" data-toggle="tab" aria-controls="step4"
                                                            role="tab" title="@lang('admin.features')">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-tags"></i>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li role="presentation" class="disabled">
                                                        <a href="#complete" data-toggle="tab" aria-controls="complete"
                                                            role="tab" title="@lang('admin.filters')"
                                                            style="left: -30%;">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-filter"></i>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="tab-content">
                                                @include('admin.layout.message')
                                                <form method="post" action="{{ route('entities.store') }}"
                                                    data-parsley-validate>
                                                    @csrf
                                                    @method('post')
                                                    <div class="tab-pane active" role="tabpanel" id="step1">

                                                        <div class="form-group col-md-4">
                                                            <label for="region">@lang('admin.region') :</label>
                                                            <select class="form-control select2me" id="region"
                                                                name="region_id" required>
                                                                <option value="">---</option>
                                                                @foreach ($regions as $region)
                                                                <option value="{{$region->id}}">{{$region->title}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label for="admin">@lang('admin.admin') :</label>
                                                            <select class="form-control select2me" id="admin"
                                                                name="admin_id" required>
                                                                <option value="">---</option>
                                                                @foreach ($admins as $admin)
                                                                <option value="{{$admin->id}}">{{$admin->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label for="region">@lang('admin.category') :</label>
                                                            <select class="form-control select2me" id="category"
                                                                name="cat_id" required>
                                                                <option value="">---</option>
                                                                @foreach ($categories as $category)
                                                                <option value="{{$category->id}}">{{$category->title}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label for="code">@lang('admin.unitcode')</label>
                                                            <input type="text" name="code" value="{{old('code')}}"
                                                                required parsley-trigger="change"
                                                                placeholder="@lang('admin.unitcode')"
                                                                class="form-control">
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label for="space">@lang('admin.space')</label>
                                                            <input type="number" name="space" value="{{old('space')}}"
                                                                required parsley-trigger="change"
                                                                placeholder="@lang('admin.space')" class="form-control">
                                                        </div>

                                                        <div class="form-group col-md-2"
                                                            style="text-align: center;margin-top: 28px;">
                                                            <input type="checkbox" id="todo" name="family" value="1"
                                                                style="margin: 0px;">
                                                            <label for="todo"
                                                                data-content="@lang('admin.family')">@lang('admin.family')</label>
                                                        </div>

                                                        <div class="form-group col-md-2"
                                                            style="text-align: center;margin-top: 28px;">
                                                            <input type="checkbox" id="todo" name="showcode" value="1"
                                                                style="margin: 0px;">
                                                            <label for="todo"
                                                                data-content="@lang('admin.showcode')">@lang('admin.showcode')</label>
                                                        </div>

                                                        @foreach (config('translatable.locales') as $locale)
                                                        <div class="form-group col-md-6">
                                                            <label for="title">@lang('admin.' .$locale.
                                                                '.title')</label>
                                                            <input type="text" name="{{$locale}}[title]"
                                                                value="{{old($locale . '.title')}}" required
                                                                parsley-trigger="change"
                                                                placeholder="@lang('admin.'.$locale .'.title')"
                                                                class="form-control">
                                                        </div>
                                                        @endforeach


                                                        @foreach (config('translatable.locales') as $locale)
                                                        <div class="form-group col-md-6">
                                                            <label for="description">@lang('admin.' .$locale.
                                                                '.description')</label>
                                                            <textarea type="text" name="{{$locale}}[description]"
                                                                value="{{old($locale . '.description')}}" required
                                                                parsley-trigger="change"
                                                                placeholder="@lang('admin.'.$locale .'.description')"
                                                                class="form-control"></textarea>
                                                        </div>
                                                        @endforeach

                                                        @foreach (config('translatable.locales') as $locale)
                                                        <div class="form-group col-md-6">
                                                            <label for="address">@lang('admin.' .$locale.
                                                                '.address')</label>
                                                            <textarea type="text" name="{{$locale}}[address]"
                                                                value="{{old($locale . '.address')}}" required
                                                                parsley-trigger="change"
                                                                placeholder="@lang('admin.'.$locale .'.address')"
                                                                class="form-control"></textarea>
                                                        </div>
                                                        @endforeach

                                                        <ul class="list-inline pull-right">
                                                            <li>
                                                                @if(Auth::guard('admin')->user()->hasPermission('entities-create'))
                                                                <button type="submit"
                                                                    class="btn btn-primary">@lang('admin.add')</button>
                                                                @else
                                                                <button type="submit" class="btn btn-primary"
                                                                    disabled>@lang('admin.add')</button>
                                                                @endif
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </form>

                                                <div class="clearfix"></div>
                                            </div>

                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
