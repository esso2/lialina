@extends('admin.layout.master')

@section('content')

<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.entities')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.entities')</a></li>
                            <li class="active">@lang('admin.addentities')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="container">
                                <div class="row">
                                    <section>
                                        <div class="wizard">
                                            <div class="wizard-inner">
                                                <div class="connecting-line"></div>
                                                <ul class="nav nav-tabs" role="tablist">

                                                    <li role="presentation" class="active">
                                                        <a href="#step1" data-toggle="tab" aria-controls="step1"
                                                            role="tab" title="@lang('admin.maininfo')">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-home"></i>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li role="presentation" class="">
                                                        <a href="#step2" data-toggle="tab" aria-controls="step2"
                                                            role="tab" title="@lang('admin.details')">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-tasks"></i>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li role="presentation" class="">
                                                        <a href="#step3" data-toggle="tab" aria-controls="step3"
                                                            role="tab" title="@lang('admin.images')">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-picture"></i>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="">
                                                        <a href="#step4" data-toggle="tab" aria-controls="step4"
                                                            role="tab" title="@lang('admin.features')">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-tags"></i>
                                                            </span>
                                                        </a>
                                                    </li>

                                                    <li role="presentation" class="">
                                                        <a href="#complete" data-toggle="tab" aria-controls="complete"
                                                            role="tab" title="@lang('admin.filters')"
                                                            style="left: -30%;">
                                                            <span class="round-tab">
                                                                <i class="glyphicon glyphicon-filter"></i>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active" role="tabpanel" id="step1">
                                                    <form id="entities" method="post" data-parsley-validate>
                                                        @csrf
                                                        @method('put')
                                                        <input type="hidden" name="EntityId" id="entity_id" value="{{$entity->id}}"  class="form-control">
                                                    <div class="form-group col-md-4">
                                                        <label for="region">@lang('admin.region') :</label>
                                                        <select class="form-control select2me" id="region"
                                                            name="region_id">
                                                            <option value="0">---</option>
                                                            @foreach ($regions as $region)
                                                            <option value="{{$region->id}}" @if($region->id == $entity->region_id) selected @endif> {{$region->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="region">@lang('admin.admin') :</label>
                                                        <select class="form-control select2me" id="admin"
                                                            name="admin_id">
                                                            <option value="0">---</option>
                                                            @foreach ($admins as $admin)
                                                            <option value="{{$admin->id}}" @if($admin->id == $entity->admin_id) selected @endif> {{$admin->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="region">@lang('admin.category') :</label>
                                                        <select class="form-control select2me" id="region"
                                                            name="cat_id">
                                                            <option value="0">---</option>
                                                            @foreach ($categories as $category)
                                                            <option value="{{$category->id}}" @if($category->id == $entity->cat_id) selected @endif> {{$category->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="code">@lang('admin.unitcode')</label>
                                                        <input type="text" name="code" value="{{$entity->code}}"
                                                            required parsley-trigger="change"
                                                            placeholder="@lang('admin.unitcode')"
                                                            class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="space">@lang('admin.space')</label>
                                                        <input type="number" name="space" value="{{$entity->space}}"
                                                            required parsley-trigger="change"
                                                            placeholder="@lang('admin.space')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-2"
                                                        style="text-align: center;margin-top: 28px;">
                                                        <input type="checkbox" id="todo" name="family" value="{{$entity->family}}"
                                                            style="margin: 0px;" {{ $entity->family == 1 ? 'checked' : null }}>
                                                        <label for="todo"
                                                            data-content="@lang('admin.family')">@lang('admin.family')</label>
                                                    </div>

                                                    <div class="form-group col-md-2"
                                                        style="text-align: center;margin-top: 28px;">
                                                        <input type="checkbox" id="todo" name="showcode" value="{{$entity->show_code}}"
                                                            style="margin: 0px;" {{ $entity->show_code == 1 ? 'checked' : null }}>
                                                        <label for="todo"
                                                            data-content="@lang('admin.showcode')">@lang('admin.showcode')</label>
                                                        </div>
                                                    @foreach (config('translatable.locales') as $locale)
                                                    <div class="form-group col-md-6">
                                                        <label for="title">@lang('admin.' .$locale. '.title')</label>
                                                        <input type="text" name="{{$locale}}[title]"
                                                            value="{{$entity->translate($locale)->title}}" 
                                                            required parsley-trigger="change" 
                                                            placeholder="@lang('admin.'.$locale .'.title')" 
                                                            class="form-control" >
                                                    </div>
                                                   @endforeach


                                                    @foreach (config('translatable.locales') as $locale)
                                                    <div class="form-group col-md-6">
                                                        <label for="description">@lang('admin.' .$locale.
                                                            '.description')</label>
                                                        <textarea type="text" name="{{$locale}}[description]"
                                                             required
                                                            parsley-trigger="change"
                                                            placeholder="@lang('admin.'.$locale .'.description')"
                                                            class="form-control">{{$entity->translate($locale)->description}}</textarea>
                                                    </div>
                                                    @endforeach

                                                    @foreach (config('translatable.locales') as $locale)
                                                    <div class="form-group col-md-6">
                                                        <label for="address">@lang('admin.' .$locale.
                                                            '.address')</label>
                                                        <textarea type="text" name="{{$locale}}[address]"
                                                            required
                                                            parsley-trigger="change"
                                                            placeholder="@lang('admin.'.$locale .'.address')"
                                                            class="form-control">{{$entity->translate($locale)->address}}</textarea>
                                                    </div>
                                                    @endforeach


                                                    <ul class="list-inline pull-right">
                                                        <li><button type="button" class="btn btn-success next-step">@lang('admin.next')</button></li>
                                                        @if(Auth::guard('admin')->user()->hasPermission('entities-update'))
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit" name="submit">@lang('admin.save')</button>
                                                        @else
                                                        <button class="btn btn-primary waves-effect waves-light" disabled>@lang('admin.save')</button>
                                                        @endif
                                                    </ul>
                                                </form>
                                            </div>


                                                <div class="tab-pane" role="tabpanel" id="step2">
                                                    <form id="entitiesStep2" method="post" data-parsley-validate>
                                                        @csrf
                                                        @method('put')
                                                        <input type="hidden" name="EntityId" id="entity_id" value="{{$entity->id}}"  class="form-control">
                                                    <div class="form-group col-md-4">
                                                        <label for="price">@lang('admin.price') : </label>
                                                        <input type='number' name="price" value="{{$entity->detailes->price ?? ''}}"
                                                            required parsley-trigger="change" 
                                                            placeholder="@lang('admin.price')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="price_end">@lang('admin.price_end') : </label>
                                                        <input type="number" name="price_weekend"
                                                            value="{{$entity->detailes->price_weekend ?? ''}}" required
                                                            parsley-trigger="change"
                                                            placeholder="@lang('admin.price_end')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="deposit">@lang('admin.deposit') : </label>
                                                        <input type="number" name="deposit" value="{{$entity->detailes->deposit ?? ''}}"
                                                            required parsley-trigger="change"
                                                            placeholder="@lang('admin.deposit')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="mobile">@lang('admin.mobile') : </label>
                                                        <input type="text" name="mobile" value="{{$entity->detailes->mobile ?? ''}}"
                                                            required parsley-trigger="change"
                                                            placeholder="@lang('admin.mobile')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="facebook">@lang('admin.facebook') : </label>
                                                        <input type="text" name="facebook" value="{{$entity->detailes->facebook ?? ''}}"
                                                            required parsley-trigger="change"
                                                            placeholder="@lang('admin.facebook')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="instgram">@lang('admin.instgram') : </label>
                                                        <input type="text" name="instgram" value="{{$entity->detailes->instgram ?? ''}}"
                                                            required parsley-trigger="change"
                                                            placeholder="@lang('admin.instgram')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="whatsapp">@lang('admin.whatsapp') : </label>
                                                        <input type="text" name="whatsapp" value="{{$entity->detailes->whatsapp ?? ''}}"
                                                            required parsley-trigger="change"
                                                            placeholder="@lang('admin.whatsapp')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-12">
                                                        <label for="twitter">@lang('admin.twitter') : </label>
                                                        <input type="text" name="twitter" value="{{$entity->detailes->twitter ?? ''}}"
                                                            required parsley-trigger="change"
                                                            placeholder="@lang('admin.twitter')" class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <label for="check_in_am">@lang('admin.check_in_am') : </label>
                                                        <input type="time" id="check_in_am" step="2" name="check_in_am"
                                                            value="{{$entity->detailes->check_in_am ?? ''}}" required
                                                            parsley-trigger="change"
                                                            placeholder="@lang('admin.check_in_am')"
                                                            class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <label for="check_out_am">@lang('admin.check_out_am') : </label>
                                                        <input type="time" id="check_out_am" step="2" name="check_out_am"
                                                            value="{{$entity->detailes->check_out_am ?? ''}}" required
                                                            parsley-trigger="change"
                                                            placeholder="@lang('admin.check_out_am')"
                                                            class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <label for="check_in_pm">@lang('admin.check_in_pm') : </label>
                                                        <input type="time" id="check_in_pm" step="2" name="check_in_pm"
                                                            value="{{$entity->detailes->check_in_pm ?? ''}}" required
                                                            parsley-trigger="change"
                                                            placeholder="@lang('admin.check_in_pm')"
                                                            class="form-control">
                                                    </div>

                                                    <div class="form-group col-md-3">
                                                        <label for="check_out_pm">@lang('admin.check_out_pm') : </label>
                                                        <input type="time" id="check_out_pm" step="2" name="check_out_pm"
                                                            value="{{$entity->detailes->check_out_pm ?? ''}}" required
                                                            parsley-trigger="change"
                                                            placeholder="@lang('admin.check_out_pm')"
                                                            class="form-control">
                                                    </div>

                                                    <div class="form-group col-sm-12" id="map" style="height: 250px">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label>@lang('admin.lat') : </label>
                                                        <input type="text" id="lat" class="form-control" value="{{$entity->detailes->lat ?? ''}}" name="lat">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label>@lang('admin.long') :</label>
                                                        <input type="text" id="lang" class="form-control" value="{{$entity->detailes->lang ?? ''}}" name="lang">
                                                    </div>

                                                    <ul class="list-inline pull-right">
                                                        <li><button type="button"
                                                                class="btn btn-primary next-step">@lang('admin.next')</button></li>
                                                                @if(Auth::guard('admin')->user()->hasPermission('entities-update'))
                                                                <button type="submit"
                                                                    class="btn btn-primary">@lang('admin.save')</button>
                                                                @else
                                                                <button type="submit" class="btn btn-primary"
                                                                    disabled>@lang('admin.save')</button>
                                                                @endif
                                                        <li><button type="button"
                                                                class="btn btn-default prev-step">@lang('admin.previous')</button></li>
                                                    </ul>
                                                    </form>
                                                </div>


                                                <div class="tab-pane" role="tabpanel" id="step3">
                                                    <form method="post" action="{{route('entityimg')}}" enctype="multipart/form-data" 
                                                    class="dropzone" id="dropzone" style="margin: 15px;">
                                                    @csrf
                                                    <input type="hidden" name="EntityId" id="entity_id" value="{{$entity->id}}" class="form-control">
                                                    <div class="dz-message" data-dz-message><span>@lang('admin.imagebox') &#127889;</span></div>
                                                    
                                                    </form>  

                                                    <ul class="list-inline pull-right">
                                                        <li><button type="button"
                                                                class="btn btn-primary next-step">@lang('admin.next')</button></li>

                                                        <li><button type="button"
                                                                class="btn btn-default prev-step">@lang('admin.previous')</button></li>
                                                    </ul>
                                                </div>

                                                <div class="tab-pane" role="tabpanel" id="step4">
                                                    <div class="col-lg-12">
                                                    <form id="features" method="post"
                                                    data-parsley-validate>
                                                    @csrf
                                                    @method('post')
                                                    <input type="hidden" name="EntityId" id="entity_id" value="{{$entity->id}}" class="form-control">
                                                    <div class="form-group col-md-3">
                                                        <label for="region">@lang('admin.type') :</label>
                                                        <select class="form-control select2me" id="type_id" name="type_id">
                                                            <option value="0">---</option>
                                                            @foreach ($types as $key => $type)
                                                            <option value="{{$key+1}}">@lang('admin.'.$type)</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    @foreach (config('translatable.locales') as $locale)
                                                    <div class="form-group col-md-4" style="margin-top: -26px;">
                                                        <label for="title">@lang('admin.' .$locale. '.feattitle')</label>
                                                        <textarea type="text" name="{{$locale}}[title]"
                                                        required parsley-trigger="change" 
                                                        placeholder="@lang('admin.'.$locale .'.feattitle')" 
                                                        class="form-control"></textarea>
                                                    </div>
                                                    @endforeach
                                               <div class="form-group col-md-1" style="margin-top: 25px;">
                                                @if(Auth::guard('admin')->user()->hasPermission('entities-update'))
                                                <button type="submit"
                                                    class="btn btn-primary" style="margin-right: 10px;">@lang('admin.save')</button>
                                                @else
                                                <button type="submit" class="btn btn-primary" style="margin-right: 130px;"
                                                    disabled >@lang('admin.save')</button>
                                                @endif
                                               </div>
                                            </form>
                                                    </div>

                                                          <div class="col-lg-12">
                                                        <div class="card-box">
                                                            <table class="table table-bordered feature-table" style="width: 100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">#</th>
                                                                        <th class="text-center">@lang('admin.title')</th>
                                                                        <th class="text-center">@lang('admin.type')</th>
                                                                        <th class="text-center"></th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <ul class="list-inline pull-right">
                                                        <li><button type="button"
                                                                class="btn btn-primary next-step">@lang('admin.next')</button></li>
                                                               
                                                        <li><button type="button"
                                                                class="btn btn-default prev-step">@lang('admin.previous')</button></li>
                                                    </ul>
                                         
                                                
                                            </div>

                                                <div class="tab-pane" role="tabpanel" id="complete">
                                            
                                                    <input type="hidden" name="EntityId" id="entity_id" value="{{$entity->id}}" class="form-control">
                                                    <div class="col-lg-12">
                                                        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" 
                                                        data-target="#myModal" 
                                                        style="margin-right: 406px;margin-top: -46px;">@lang('admin.addfilters')</button>        
                                                        
                                                        <div id="myModal" class="modal fade" role="dialog">
                                                            <div class="modal-dialog">
                                                              <div class="modal-content">
                                                                  <form id="filters" method="post" data-parsley-validate >
                                                                  @csrf
                                                                  @method('post')
                                                                 
                                                                <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                  <h4 class="modal-title" style="text-align: center !important;">@lang('admin.filters')</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                        @foreach (config('translatable.locales') as $locale)
                                                                        <div class="form-group col-md-6">
                                                                            <label for="title">@lang('admin.' .$locale.
                                                                                '.termtitle')</label>
                                                                            <input type="text" name="{{$locale}}[title]"
                                                                                value="{{old($locale . '.title')}}" required
                                                                                parsley-trigger="change"
                                                                                placeholder="@lang('admin.'.$locale .'.termtitle')"
                                                                                class="form-control">
                                                                        </div>
                                                                        @endforeach

                                              
                                                                       <div class="input_fields_container">
                                                                       
                                                                            @foreach (config('translatable.locales') as $locale)
                                                                                <input type="text" name="{{$locale}}[option_title][]"
                                                                                    value="{{old($locale . '.title')}}" required
                                                                                    parsley-trigger="change"
                                                                                    placeholder="@lang('admin.'.$locale .'.filter')"
                                                                                    >
                                                                            @endforeach
                                                                            <button class="btn btn-sm btn-primary add_more_button"><i class="glyphicon glyphicon-plus-sign"></i></button>
                                                                      </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                  <button type="button" class="btn btn-default" data-dismiss="modal">@lang('admin.close')</button>
                                                                  <button type="submit" class="btn btn-primary">@lang('admin.save')</button>
                                                                </div>
                                                            </form>
                                                              </div>
                                                            </div>
                                                          </div>
                                                    </div>
                                                    
                                                    {{-- {{dd($entity->filteroptions)}} --}}
                                                        <div class="col-lg-12" id="mydiv">
                                                            
                                                            @if (!empty($filters))
                                                            @foreach ($filters as $key => $item)
                                                            <div class="d-flex justify-content-center col-md-4">
                                                                <div class="wrapper center-block" style="padding-top: 40px;">
                                                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 25px;">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading" role="tab" id="heading{{$key}}">
                                                                                <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" aria-expanded="false" aria-controls="collapse{{$key}}"> {{$item->title}} </a>  <form class=" delete"  action="{{route('filterDelete', $item->id)}}"  method="POST" id="deloptionform"
                                                                                    style="float: left;margin-top: -20px;margin-left: -20px;">
                                                                                    <input name="_method" type="hidden" value="DELETE">
                                                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                                                    <button type="submit" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o"></i></button>
                                                                                    </form></h4>
                                                                            </div>
                                                                            <div id="collapse{{$key}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$key}}">
                                                                                <div class="panel-body">
                                                                                @if (!empty($item->filteroptions))
                                                                                    @foreach ($item->filteroptions as $key => $option)
                                                                                <div class="col-lg-12">
                                                                                    <label> <input id="optionCheck" data-id="{{$option->id}}" type="checkbox" name="filter[]" value="12"
                                                                                        @foreach ($entity->filteroptions as $key => $option2){{ $option2->pivot->filter_option_id == $option->id ? 'checked' : null }} @endforeach/> <span class="ml-10">{{$option->title}}</span> </label>
                                                                                    <form class=" delete"  action="{{route('optionDelete', $option->id)}}"  method="POST" id="deloptionform"
                                                                                    style="display: inline-block; right: 50px;" >
                                                                                    <input name="_method" type="hidden" value="DELETE">
                                                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                                                    <button type="submit" class="btn btn-danger btn-xs" title="Delete"><i class="fa fa-trash-o"></i></button>
                                                                                    </form>
                                                                                </div>
                                                                                    @endforeach
                                                                                @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                            @endif    
                                                        </div>

                                                        <ul class="list-inline pull-right">
                                                            
                                                            <li><button type="button"
                                                                    class="btn btn-default prev-step">@lang('admin.previous')</button></li>
                                                        </ul>
                                              
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                           
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

<script>
    $('body').on('click','#optionCheck',function () {
     var entity_id = $('#entity_id').val();
     var option_id = $(this).data('id');
      $.ajax({
        url:'{{ route('entityoptionadd') }}',
        type:'post',
        data:{
            'entity_id':entity_id,
            'option_id':option_id
        },
         success: function (response) {
         if (response.status == 'success'){
             new Noty({
                type: 'success',
                layout: 'topRight',
                text: "@lang('admin.statuschange')",
                timeout: 5000,
                killer: true
              }).show();
            }
        }
    });
});
</script>

<script>
    $(document).ready(function() {
    var max_fields_limit      = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function(e){ //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
            x++; //counter increment
            $('.input_fields_container').
            append('<div><input type="text" name="ar[option_title][]" placeholder="@lang('admin.ar.filter')" style="margin-top: 10px;" required/> <input type="text" name="en[option_title][]" placeholder="@lang('admin.en.filter')" style="margin-top: 10px;" required/><a href="#" class="remove_field" style="margin-left:10px;"> <button class="btn btn-sm btn-danger add_more_button"><i class="glyphicon glyphicon-trash"></i></button></a></div>'); //add input field
        }
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

<script>
        var locale = '{{ config('app.locale') }}';
    //console.log(locale);
    if (locale == 'ar') {
        var table = $('.feature-table').DataTable({
       "language": {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Arabic.json"},
       processing: true,
       serverSide: true,
       ajax: {
       "url": "{{ route('entityfeatall') }}",
       "data": {
       "id": $('#entity_id').val(),
            }
        },
       columns: [
           {data: 'id', name: 'id'},
           {data: 'title', name: 'title',searchable: true, sortable : true},
           {data: 'type', name: 'type_id',searchable: true, sortable : true},
           {data: 'action', name: 'action', orderable: false, searchable: false},
       ],

       responsive:true,
       order:[0,'desc']
   });      
        } else {
            var table = $('.feature-table').DataTable({
       
       "language": {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json"},
       processing: true,
       serverSide: true,
       ajax: {
       "url": "{{ route('entityfeatall') }}",
       "data": {
       "id": $('#entity_id').val(),
            }
        },
       columns: [
           {data: 'id', name: 'id'},
           {data: 'title', name: 'title',searchable: true, sortable : true},
           {data: 'type', name: 'type_id',searchable: true, sortable : true},
           {data: 'action', name: 'action', orderable: false, searchable: false},
       ],

       responsive:true,
       order:[0,'desc']
   }); 

}
</script>
<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('body').on('submit', '#entities', function (e) {
            e.preventDefault();
            //alert('asdasd');
            let id = $('#entity_id').val();
            var url = '{{ route('entities.update', ':id') }}';
              url = url.replace(':id', id);
            $.ajax({
                url: url,
                method: "post",
                data: new FormData(this),
                dataType: 'json',
                cache       : false,
                contentType : false,
                processData : false,

                success: function (response) {
                    //console.log(response);
                    if (response.errors){
                        $.each(response.errors, function( index, value ) {
                            new Noty({
                                type: 'error',
                                layout: 'topRight',
                                text: value,
                                timeout: 2000,
                                killer: true
                            }).show();
                        });
                     }

                    if (response.status == 'success') {
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: "@lang('admin.addsuccessfully')",
                            timeout: 5000,
                            killer: true
                        }).show();
                    }
                },

            });
        });

        $('body').on('submit', '#entitiesStep2', function (e) {
            e.preventDefault();
           // alert('asdasd22');
            let id = $('#entity_id').val();
            var url = '{{ route('updatestep2', ':id') }}';
              url = url.replace(':id', id);
            $.ajax({
                url: url,
                method: "post",
                data: new FormData(this),
                dataType: 'json',
                cache       : false,
                contentType : false,
                processData : false,

                success: function (response) {
                    if (response.errors){
                        $.each(response.errors, function( index, value ) {
                            new Noty({
                                type: 'error',
                                layout: 'topRight',
                                text: value,
                                timeout: 2000,
                                killer: true
                            }).show();
                        });
                     }
                    if (response.status == 'success') {
                        new Noty({
                            type: 'success',
                            layout: 'topRight',
                            text: "@lang('admin.addsuccessfully')",
                            timeout: 5000,
                            killer: true
                        }).show();
                    }
                },

            });
        });

        $('body').on('submit','#features',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                $.ajax({
                    url: '{{ route('entityfeatadd')}}',
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.addsuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                            table.ajax.reload();
                        }
                    },

                });
            });


            $('body').on('submit','#filters',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                $.ajax({
                    url: '{{ route('entityfiltadd')}}',
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.addsuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                            $('#myModal').modal('hide');
                            $("#mydiv").load(" #mydiv");
                        }
                    },

                });
            });

    });

    $('body').on('submit','#deloptionform',function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        $.ajax({
          url: url,
          method: "delete",
          data: {
              _token: '{{ csrf_token() }}',
          },
          success: function (response) {

              if (response.status == 'success'){
                  new Noty({
                      type: 'success',
                      layout: 'topRight',
                      text: "@lang('admin.deletesuccessfully')",
                      timeout: 5000,
                      killer: true
                  }).show();
                  $("#mydiv").load("#mydiv");
              }
          }
        });
        })

        $('body').on('submit','#delform',function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        $.ajax({
          url: url,
          method: "delete",
          data: {
              _token: '{{ csrf_token() }}',
          },
          success: function (response) {

              if (response.status == 'success'){
                  new Noty({
                      type: 'success',
                      layout: 'topRight',
                      text: "@lang('admin.deletesuccessfully')",
                      timeout: 5000,
                      killer: true
                  }).show();
                  table.ajax.reload();
              }
          }
        });
        })

</script>

<script>
    var map;
    var markers = [];

    function initMap() {
        var haightAshbury = {
            lat: {{$entity->detailes->lat ?? '26.22170100683176'}},
            lng: {{$entity->detailes->lang ?? '50.58556788820532'}}
        };

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: haightAshbury,
            mapTypeId: 'terrain'
        });

        $('#lat').val('{{$entity->detailes->lat ?? 26.22170100683176}}');
        $('#lang').val('{{$entity->detailes->lang ?? 50.58556788820532}}');
        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function (event) {
            addMarker(event.latLng);
            var latitude = event.latLng.lat();
            var longitude = event.latLng.lng();
            $('#lat').val(latitude);
            $('#lang').val(longitude);

        });

        // Adds a marker at the center of the map.
        addMarker(haightAshbury);
    }

    // Adds a marker to the map and push to the array.
    function addMarker(location) {
        clearMarkers();
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        markers.push(marker);
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

</script>

<script>
    $(document).ready(function () {


        $("#lat").on("input", function () {
            // // Print entered value in a div box
            var lat = $("#lat").val();
            var lang = $("#lang").val();

            var haightAshbury = {
                lat: 26.22170100683176,
                lng: 50.58556788820532
            };
            haightAshbury["lat"] = Number(lat);
            haightAshbury["lng"] = Number(lang);

            // Adds a marker at the center of the map.
            addMarker(haightAshbury);


            console.log(haightAshbury);
        });


        $("#lang").on("input", function () {
            // // Print entered value in a div box
            var lat = $("#lat").val();
            var lang = $("#lang").val();

            var haightAshbury = {
                lat: 26.22170100683176,
                lng: 50.58556788820532
            };
            haightAshbury["lat"] = Number(lat);
            haightAshbury["lng"] = Number(lang);

            // Adds a marker at the center of the map.
            addMarker(haightAshbury);


            console.log(haightAshbury);
        });



        $("#cssmenu ul>li").removeClass("active");
        $("#item8").addClass("active");
    });

</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDeuOTzbTLT2EHbvjzyHCckOiIpnTRZjSY&callback=initMap">
</script>

    <script type="text/javascript">
     var locale = '{{ config('app.locale') }}';
     if (locale == 'ar') {
       Dropzone.options.dropzone =
     {
            
            maxFilesize: 12,
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
               return time+file.name;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            autoProcessQueue: true,
            dictRemoveFile: "حذف الصورة", 
            timeout: 50000,
    
            removedfile: function(file, response) 
            {
                var name = file.name;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('entityimgdelete') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        name: name,
                    },
                    success: function (data){
                        //console.log(data);
                        new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: "@lang('admin.deletesuccessfully')",
                        timeout: 5000,
                        killer: true
                    }).show();
                    },
                    error: function(e) {
                        new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: "@lang('admin.notdeletesuccessfully')",
                        timeout: 5000,
                        killer: true
                    }).show();
                    }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ? 
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },

            init: function() {
            var thisDropzone = this;
            var entity_id = $('#entity_id').val();
            console.log(entity_id)
            $.ajax({
            dataType: "json",
            url:'{{ route('entityimgshow') }}',
            type:'GET',
            data:{
                'entity_id': entity_id
            },
                success: function (response) {
                    $.each(response, function(key,value){ //loop through it
                    var mockFile = { name: value.name.image, size: value.size ,url: value.name.image_path};
                    thisDropzone.files.push(mockFile);
                    thisDropzone.emit('addedfile', mockFile);
                    thisDropzone.emit("thumbnail", mockFile, mockFile.url);
                    thisDropzone.emit('complete', mockFile);
                    });
                }
            });

          },
       
            success: function(file, response) 
            {
                //console.log(file);
                if(response.status == 'success'){
                    new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: "@lang('admin.uplodsuccessfully')",
                        timeout: 5000,
                        killer: true
                    }).show();
                        }
            },
            error: function(file, response)
            {
                new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: "@lang('admin.notuplodsuccessfully')",
                        timeout: 5000,
                        killer: true
                    }).show();
            }
    };   
    
     }else{
        Dropzone.options.dropzone =
     {
            
            maxFilesize: 12,
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
               return time+file.name;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            autoProcessQueue: true,
            dictRemoveFile: "Remove Image", 
            timeout: 50000,
    
            removedfile: function(file, response) 
            {
                var name = file.name;
                $.ajax({
                    type: 'POST',
                    url: '{{ route('entityimgdelete') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        name: name,
                    },
                    success: function (data){
                        //console.log(data);
                        new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: "@lang('admin.deletesuccessfully')",
                        timeout: 5000,
                        killer: true
                    }).show();
                    },
                    error: function(e) {
                        new Noty({
                        type: 'error',
                        layout: 'topRight',
                        text: "@lang('admin.notdeletesuccessfully')",
                        timeout: 5000,
                        killer: true
                    }).show();
                    }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ? 
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },

            init: function() {
            var thisDropzone = this;
            var entity_id = $('#entity_id').val();
            console.log(entity_id)
            $.ajax({
            dataType: "json",
            url:'{{ route('entityimgshow') }}',
            type:'GET',
            data:{
                'entity_id': entity_id
            },
                success: function (response) {
                    $.each(response, function(key,value){ //loop through it
                    var mockFile = { name: value.name.image, size: value.size ,url: value.name.image_path};
                    thisDropzone.files.push(mockFile);
                    thisDropzone.emit('addedfile', mockFile);
                    thisDropzone.emit("thumbnail", mockFile, mockFile.url);
                    thisDropzone.emit('complete', mockFile);
                    });
                }
            });

          },
       
            success: function(file, response) 
            {
                //console.log(file);
                if(response.status == 'success'){
                    new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: "@lang('admin.uplodsuccessfully')",
                        timeout: 5000,
                        killer: true
                    }).show();
                    $("#imgdiv").load(" #imgdiv");
                        }
            },
            error: function(file, response)
            {
                new Noty({
                        type: 'success',
                        layout: 'topRight',
                        text: "@lang('admin.notuplodsuccessfully')",
                        timeout: 5000,
                        killer: true
                    }).show();
            }
    }; 
     }
    
</script>

<script>
    document.getElementById("check_in_am").step = "10";
    document.getElementById("check_out_am").step = "10";
    document.getElementById("check_in_pm").step = "10";
    document.getElementById("check_out_pm").step = "10";
</script>
@endpush
