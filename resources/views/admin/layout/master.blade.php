<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    <head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">
<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{asset('AdminS/assets_ar/images/favicon_1.ico')}}">

    <title></title>

        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Changa:wght@400;700&display=swap" rel="stylesheet">
        <style>
            * {
                font-family: 'Changa', sans-serif;
            }

            h1, h2, h3, h4, h5, h6 {
                font-family: 'Changa', sans-serif !important;
            }
        </style>

        @if (app()->getlocale() == 'ar')

    <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/switchery/css/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/multiselect/css/multi-select.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/bootstrap-select/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

    <link href="{{asset('AdminS/assets_ar/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/core.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/components.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/icons.css ')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('AdminS/assets_ar/css/pages.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/menu.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/steps.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('AdminS/assets_ar/plugins/fullcalendar/css/fullcalendar.min.css')}}">

@else
            
        <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/sweetalert/dist/sweetalert.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/switchery/css/switchery.min.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/multiselect/css/multi-select.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/select2/css/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/bootstrap-select/css/bootstrap-select.min.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/bootstrap-timepicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">
    
        <link href="{{asset('AdminS/assets_en/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('AdminS/assets_en/css/core.css')}}" rel="stylesheet">
        <link href="{{asset('AdminS/assets_en/css/components.css')}}" rel="stylesheet">
        <link href="{{asset('AdminS/assets_en/css/pages.css')}}" rel="stylesheet">
        <link href="{{asset('AdminS/assets_en/css/icons.css ')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('AdminS/assets_en/css/menu.css')}}" rel="stylesheet">
        <link href="{{asset('AdminS/assets_en/css/responsive.css')}}" rel="stylesheet">
        <link href="{{asset('AdminS/assets_en/css/steps.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('AdminS/assets_en/plugins/fullcalendar/css/fullcalendar.min.css')}}">
   

        @endif
 @stack('css')

     {{--noty--}}
     <link rel="stylesheet" href="{{ asset('AdminS/noty/noty.css') }}">
     <script src="{{ asset('AdminS/noty/noty.min.js') }}"></script>

     {{--parsly--}}
     <link rel="stylesheet" href="{{ asset('AdminS/parsley/parsly.css') }}">
    </head>
<body>

    <!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container">
            <!-- Logo container-->
            <div class="logo">
                <a href="index.php" class="logo">
                    <span></span>
                </a>
            </div>
            <!-- End Logo container-->
            <div class="menu-extras">
                <ul class="nav navbar-nav navbar-right pull-right">

                    <li class="dropdown navbar-c-items">
                        <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="" alt="user-img" class="img-circle"> </a>
                        <ul class="dropdown-menu">
                            <li><a href=""><i class="ti-power-off text-danger m-r-10"></i></a></li>
                        </ul>
                    </li>
                </ul>
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

        </div>
    </div>

    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li class="has-submenu">
                        <a href="{{route('adminhome')}}"><i class="md md-dashboard"></i> @lang('admin.dashboard')</a>
                    </li>

                    @if(Auth::guard('admin')->user()->hasPermission('entities-read'))
                    <li id="item3" class="has-submenu">
                        <a href="#"><i class="md md-class"></i> @lang('admin.entities')</a>
                        <ul class="submenu">
                            @if(Auth::guard('admin')->user()->hasPermission('entities-create'))
                              <li><a href="{{route('entities.create')}}">@lang('admin.addentities')</a></li>
                            @endif
                            
                            @if(Auth::guard('admin')->user()->hasPermission('entities-read'))
                              <li><a href="{{route('entities.index')}}">@lang('admin.entities')</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::guard('admin')->user()->hasPermission('reserves-read'))
                    <li id="item3" class="has-submenu">
                        <a href="{{route('reserves.index')}}"><i class="md md-class"></i> @lang('admin.reserves')</a>
                    </li>
                    @endif
                    
                    @if(Auth::guard('admin')->user()->hasPermission('categories-read'))
                    <li id="item3" class="has-submenu">
                        <a href="#"><i class="md md-class"></i> @lang('admin.categories')</a>
                        <ul class="submenu">
                            @if(Auth::guard('admin')->user()->hasPermission('categories-create'))
                              <li><a href="{{route('categories.create')}}">@lang('admin.addcategory')</a></li>
                            @endif
                            
                            @if(Auth::guard('admin')->user()->hasPermission('categories-read'))
                              <li><a href="{{route('categories.index')}}">@lang('admin.categories')</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::guard('admin')->user()->hasPermission('users-read'))
                    <li id="item3" class="has-submenu">
                        <a href="#"><i class="md md-class"></i> @lang('admin.users')</a>
                        <ul class="submenu">
                            @if(Auth::guard('admin')->user()->hasPermission('users-create'))
                              <li><a href="{{route('users.create')}}">@lang('admin.addusers')</a></li>
                            @endif
                            
                            @if(Auth::guard('admin')->user()->hasPermission('users-read'))
                              <li><a href="{{route('users.index')}}">@lang('admin.users')</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::guard('admin')->user()->hasPermission('roles-read'))
                    <li id="item3" class="has-submenu">
                        <a href="#"><i class="md md-class"></i> @lang('admin.roles')</a>
                        <ul class="submenu">
                            @if(Auth::guard('admin')->user()->hasPermission('roles-create'))
                            <li><a href="{{route('roles.create')}}">@lang('admin.addroles')</a></li>
                            @endif

                            @if(Auth::guard('admin')->user()->hasPermission('roles-read'))
                            <li><a href="{{route('roles.index')}}">@lang('admin.roles')</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::guard('admin')->user()->hasPermission('terms-read'))
                    <li id="item3" class="has-submenu">
                        <a href="#"><i class="md md-class"></i> @lang('admin.terms')</a>
                        <ul class="submenu">
                            @if(Auth::guard('admin')->user()->hasPermission('terms-create'))
                            <li><a href="{{route('terms.create')}}">@lang('admin.addterms')</a></li>
                            @endif

                            @if(Auth::guard('admin')->user()->hasPermission('terms-read'))
                            <li><a href="{{route('terms.index')}}">@lang('admin.terms')</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::guard('admin')->user()->hasPermission('admins-read'))
                    <li id="item3" class="has-submenu">
                        <a href="#"><i class="md md-class"></i> @lang('admin.admins')</a>
                        <ul class="submenu">
                            @if(Auth::guard('admin')->user()->hasPermission('admins-create'))
                            <li><a href="{{route('admins.create')}}">@lang('admin.addadmins')</a></li>
                            @endif

                            @if(Auth::guard('admin')->user()->hasPermission('admins-read'))
                            <li><a href="{{route('admins.index')}}">@lang('admin.admins')</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::guard('admin')->user()->hasPermission('aboutus-read'))
                    <li id="item3" class="has-submenu">
                        <a href="#"><i class="md md-class"></i> @lang('admin.aboutus')</a>
                        <ul class="submenu">
                            @if(Auth::guard('admin')->user()->hasPermission('aboutus-create'))
                            <li><a href="{{route('aboutus.create')}}">@lang('admin.addaboutus')</a></li>
                            @endif

                            @if(Auth::guard('admin')->user()->hasPermission('aboutus-read'))
                            <li><a href="{{route('aboutus.index')}}">@lang('admin.aboutus')</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if(Auth::guard('admin')->user()->hasPermission('regions-read'))
                    <li id="item3" class="has-submenu">
                        <a href="#"><i class="md md-class"></i> @lang('admin.regions')</a>
                        <ul class="submenu">
                            @if(Auth::guard('admin')->user()->hasPermission('regions-create'))
                            <li><a href="{{route('regions.create')}}">@lang('admin.addregions')</a></li>
                            @endif

                            @if(Auth::guard('admin')->user()->hasPermission('regions-read'))
                            <li><a href="{{route('regions.index')}}">@lang('admin.regions')</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif



                    @if(Auth::guard('admin')->user()->hasPermission('contactus-read'))
                    <li id="item3" class="has-submenu">
                        <a href="{{route('contactus.index')}}"><i class="md md-class"></i> @lang('admin.contactus')</a>
                    </li>
                     @endif
                    
                     @if(Auth::guard('admin')->user()->hasPermission('settings-read'))
                    <li id="item3" class="has-submenu">
                        <a href="{{route('setting')}}"><i class="md md-class"></i> @lang('admin.settings')</a>
                    </li>
                     @endif



                    <li>
                        <?php $localeCodear = 'ar' ?>
                        <?php $localeCodeen = 'en' ?>

                       @if (app()->getlocale() == 'ar')
            <a rel="alternate" hreflang="{{ $localeCodeen }}" href="{{ LaravelLocalization::getLocalizedURL($localeCodeen, null, [], true) }}"><i class="md md-language"></i> <span style="color: black;">English</span>
                        </a>                     
                        @else
            <a rel="alternate" hreflang="{{ $localeCodear }}" href="{{ LaravelLocalization::getLocalizedURL($localeCodear, null, [], true) }}"><i class="md md-language"></i> <span style="color: black;">العربيه</span></a>
                        @endif
                    </li>
              
                        <li>
            <a  href="{{ route('admin.logout') }}"
                            onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            <i class="md md-swap-vert"></i>@lang('admin.logout')</a>
    
                         <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" class="d-none">
                             @csrf
                         </form>
                        </li>

                        
                </ul>
                <!-- End navigation menu -->
            </div>
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->
        
    
<section>

        @yield('content')

</section>

@include('admin.layout.session')

<footer class="footer text-right">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    
                </div>
                
            </div>
        </div>
</footer>
@if (app()->getlocale() == 'ar')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="{{asset('AdminS/assets_ar/js/bootstrap.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/detect.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/detect.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/waves.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/wow.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/steps.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.scrollTo.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_ar/plugins/switchery/js/switchery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_ar/plugins/multiselect/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_ar/plugins/jquery-quicksearch/jquery.quicksearch.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_ar/plugins/select2/js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_ar/plugins/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_ar/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_ar/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_ar/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<!-- plugins js -->
<script src="{{asset('AdminS/assets_ar/plugins/moment/moment.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<!-- App core js -->
<script src="{{asset('AdminS/assets_ar/js/jquery.core.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.app.js')}}"></script>

<script src="{{asset('AdminS/parsley/parsley.min.js')}}"></script>
<script src="{{asset('AdminS/i18n/ar.js')}}"></script>
<!-- page js -->
<script src="{{asset('AdminS/assets_ar/pages/jquery.form-pickers.init.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/bootbox/bootbox.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/bootbox/ui-alert-dialog-api.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/jquery-knob/jquery.knob.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/plugins/fullcalendar/js/fullcalendar.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/fullcalender/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
</head>
    
@else
    
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="{{asset('AdminS/assets_en/js/bootstrap.min.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/detect.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/detect.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/waves.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/wow.min.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/steps.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/jquery.scrollTo.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_en/plugins/switchery/js/switchery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_en/plugins/multiselect/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_en/plugins/jquery-quicksearch/jquery.quicksearch.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_en/plugins/select2/js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_en/plugins/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_en/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_en/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('AdminS/assets_en/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<!-- plugins js -->
<script src="{{asset('AdminS/assets_en/plugins/moment/moment.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<!-- App core js -->
<script src="{{asset('AdminS/assets_en/js/jquery.core.js')}}"></script>
<script src="{{asset('AdminS/assets_en/js/jquery.app.js')}}"></script>


<script src="{{asset('AdminS/parsley/parsley.min.js')}}"></script>
<script src="{{asset('AdminS/i18n/en.js')}}"></script>
<!-- page js -->
<script src="{{asset('AdminS/assets_en/pages/jquery.form-pickers.init.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/bootbox/bootbox.min.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/bootbox/ui-alert-dialog-api.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/jquery-knob/jquery.knob.js')}}"></script>
<script src="{{asset('AdminS/assets_en/plugins/fullcalendar/js/fullcalendar.min.js')}}"></script>
<script src="{{asset('AdminS/assets_en/fullcalender/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
</head>
@endif

@stack('js')



<script>

 // image preview

        $(".image").change(function() {

        if (this.files && this.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('.image-show').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(this.files[0]);
        }
     });

</script>

<script>

      //delete
 $('body').on('click','.delete',function (e) {
   var that = $(this)
    e.preventDefault();

        var n = new Noty({
            text: "@lang('admin.deleteconfirm')",
            type: "warning",
            killer: true,
            buttons: [
                Noty.button("@lang('admin.yes')", 'btn btn-success mr-2', function () {
                    that.closest('form').submit();
                }),

                Noty.button("@lang('admin.no')", 'btn btn-primary mr-2', function () {
                    n.close();
                })
            ]
        });

n.show();

});//end of delete

$('.select2').select2({
    placeholder: "Select",
    width: '50% !important',
    allowClear: true
});

$('.select2me').select2({
    placeholder: "Select",
    width: '100% !important',
    allowClear: true
});

</script>
</body>
</html>
