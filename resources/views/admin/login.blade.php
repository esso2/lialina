<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    <link href="{{asset('AdminS/assets_ar/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/core.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/components.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/pages.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/menu.css')}}" rel="stylesheet">
    <link href="{{asset('AdminS/assets_ar/css/responsive.css')}}" rel="stylesheet">
    {{--noty--}}
    <link rel="stylesheet" href="{{ asset('AdminS/noty/noty.css') }}">
    <script src="{{ asset('AdminS/noty/noty.min.js') }}"></script>

    {{--parsly--}}
    <link rel="stylesheet" href="{{ asset('AdminS/parsley/parsly.css') }}">
    
<body>
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
<h3 class="text-center"> @lang('admin.login') <strong class="text-custom"> @lang('admin.management')</strong> </h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal m-t-20" method="POST" action="{{ route('admin.login') }}" data-parsley-validate>
                @csrf

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="@lang('admin.email')">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                       @enderror
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="@lang('admin.password')">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox-primary" style="text-align: center;">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember" style="margin-right: 5px;">
                                @lang('admin.remember')
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit" name="submit">@lang('admin.login')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('admin.layout.session')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="{{asset('AdminS/assets_ar/js/bootstrap.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/detect.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/detect.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/waves.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/wow.min.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('AdminS/assets_ar/js/jquery.scrollTo.min.js')}}"></script>

@if (app()->getlocale() == 'ar')
<script src="{{asset('AdminS/parsley/parsley.min.js')}}"></script>
<script src="{{asset('AdminS/i18n/ar.js')}}"></script>
@else
<script src="{{asset('AdminS/parsley/parsley.min.js')}}"></script>
<script src="{{asset('AdminS/i18n/en.js')}}"></script>
@endif
</body>
</html>
