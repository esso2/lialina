@extends('admin.layout.master')

@section('content')


<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                        <div class="col-sm-12">
                            <h4 class="page-title">@lang('admin.reserves')</h4>
                            <ol class="breadcrumb">
                                <li><a href="">@lang('admin.reserves')</a></li>
                                <li class="active">@lang('admin.reserves')</li>
                            </ol>
                        </div>
                </div>


                <div class="row">
                    <div class="col-sm-4" style="">
                        <div style="width: 32px; height: 28px; background-color: #419af5;border-radius: 9px;margin-left: 15px;"></div>
                        @if (app()->getlocale() == 'ar')
                        <h4 style="text-align: left;margin-top: -24px;margin-left: 235px;">@lang('admin.resAdmin')</h4>
                        @else
                        <h4 style="text-align: left;margin-top: -24px;margin-left: 70px;">@lang('admin.resAdmin')</h4>
                        @endif
                    </div>

                    <div class="col-sm-4">
                      <div style="width: 32px; height: 28px; display: inline-block; background-color: #29612d;border-radius: 9px;margin-left: 15px;"></div>
                      @if (app()->getlocale() == 'ar')
                      <h4 style="text-align: left;margin-top: -30px;margin-left: 235px;">@lang('admin.resUser')</h4>
                      @else
                      <h4 style="text-align: left;margin-top: -30px;margin-left: 70px;">@lang('admin.resUser')</h4>
                      @endif
                  </div>
                  
                  
                  <div class="col-sm-4">
                      <div style="width: 32px; height: 28px; display: inline-block; background-color: #ad1f1c;border-radius: 9px;margin-left: 15px;"></div>
                      @if (app()->getlocale() == 'ar')
                      <h4 style="text-align: left;margin-top: -30px;margin-left: 250px;">@lang('admin.resWating')</h4>
                      @else
                      <h4 style="text-align: left;margin-top: -30px;margin-left: 70px;">@lang('admin.resUser')</h4>
                      @endif
                  </div>
                
              </div>


        
        <div class="col-lg-12">
            <div class="card-box">
                <div id="calendar" class="col-centered" style="max-width: 100%">
                </div>
            </div>
        </div>

      <div id="ModalShow" class="modal fade myModal" role="dialog">
        <div class="modal-dialog" style="width: 100%;margin-right: -9px;">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="text-align: center !important;font-size: 
              25px; display: block ruby;">@lang('admin.reserday') : <p style="color: red;margin-left: 10px;
margin-right: 6px;"  id="start_date"></p> @lang('admin.to') : <p style="color: red;margin-left: 10px;
margin-right: 6px;"  id="end_date"></p></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered reserv-table" style="width: 100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-center">@lang('admin.title')</th>
                            <th class="text-center">@lang('admin.start')</th>
                            <th class="text-center">@lang('admin.end')</th>
                            <th class="text-center">@lang('admin.price')</th>
                            <th class="text-center">@lang('admin.paid')</th>
                            <th class="text-center">@lang('admin.remain')</th>
                            <th class="text-center">@lang('admin.user')</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">@lang('admin.cancel')</button>
              <button type="submit" id="add_resrve" class="btn btn-primary">@lang('admin.add')</button>
            </div>
          </div>
        </div>
      </div>

            </div>
        </div>
    </div>
</div>  


<div id="ModaladdResrve" class="modal fade myModal" role="dialog">
    <div class="modal-dialog" style="width: 70%;">
      <div class="modal-content">
        <form id="reserves" method="post" data-parsley-validate>
            @csrf
            @method('post')
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center !important;
          font-size: 25px; display: block ruby;"> @lang('admin.addreserves') : <p style="color: red;margin-left: 10px;
margin-right: 6px;" id="start_date2"></p>@lang('admin.to') : <p style="color: red;margin-left: 10px;
margin-right: 6px;" id="end_date2"></p> </h4>
        </div>

        <div class="modal-body"> 

            <input type="hidden" name="start_date" id="startdate">
            <input type="hidden" name="end_date" id="enddate">

              <div class="form-group col-md-4">
                    <label class="control-label">@lang('admin.entities')</label>
                        <select class="form-control select2" name="entity_id" required >
                            @foreach($entities as $entitiy)
                                <option value="{{$entitiy->id}}">{{$entitiy->title}}</option>
                            @endforeach
                        </select>
                </div>

                    <div class="form-group col-md-4">
                    <label class="control-label">@lang('admin.users')</label>
                    <select class="form-control select2" name="client_id" required >
                        @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                    </select>   
                </div> 

                <div class="form-group col-md-4">
                <label class="control-label">@lang('admin.paid')</label>
                    <input class="form-control" name="paid" required>
                </div>
        </div>
        <div class="modal-footer2">
          <button type="button" class="btn btn-default" data-dismiss="modal">@lang('admin.cancel')</button>
          <button type="submit" class="btn btn-primary">@lang('admin.save')</button>
        </div>
    </form>
      </div>
    </div>
  </div>
@endsection

@push('js')

<script>
    $(function () {
            $('body').on('submit','#reserves',function (e) {
                e.preventDefault();
                $.ajax({
                    url: '{{ route('reserves.store')}}',
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.addsuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                            window.location.reload();
                        }
                    },

                });
            });

        $('body').on('submit','#delform',function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        $.ajax({
          url: url,
          method: "delete",
          data: {
          _token: '{{ csrf_token() }}',
          },
          success: function (response) {

              if (response.status == 'success'){
                  new Noty({
                      type: 'success',
                      layout: 'topRight',
                      text: "@lang('admin.deletesuccessfully')",
                      timeout: 5000,
                      killer: true
                  }).show();
                  window.location.reload();
              }
          }
        });
        })

        });
 </script>
<script>

  $('#calendar').fullCalendar({
        eventLimit: false,
        selectable: true,
        firstDay: 6,
        longPressDelay: 0,
        //nextDayThreshold: '00:00:00',
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
           
        select: function (start,end) {
            $('#ModaladdResrve').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
            $('#ModaladdResrve').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
            $('#ModaladdResrve').attr('data-date', moment(start).format('YYYY-MM-DD'));
            $('#ModaladdResrve').attr('data-date', moment(end).format('YYYY-MM-DD'));
            var start = moment(start).format('YYYY-MM-DD');
            var end = moment(end - (24 * 3600000)).format('YYYY-MM-DD');
            var locale = '{{ config('app.locale') }}';

            $("#startdate").val(start);
            $("#enddate").val(end);

            $("#start_date").html(start);
            $("#end_date").html(end);

            $("#start_date2").html(start);
            $("#end_date2").html(end);

    if (locale == 'ar') {
    var table = $('.reserv-table').DataTable({
       "language": {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Arabic.json"},
       processing: true,
       serverSide: true,
       destroy: true,
       ajax: {
       "url": "{{ route('allreserve') }}",
       "data": {
           "start_date": start,
           "end_date": end,
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          }
        },
       columns: [
           {data: 'id', name: 'id'},
           {data: 'title', name: 'title',searchable: true, sortable : true},
           {data: 'from', name: 'from',searchable: true, sortable : true},
           {data: 'to', name: 'to',searchable: true, sortable : true},
           {data: 'price', name: 'price',searchable: true, sortable : true},
           {data: 'paid', name: 'paid',searchable: true, sortable : true},
           {data: 'remain', name: 'remain',searchable: true, sortable : true},
           {data: 'user', name: 'user',searchable: true, sortable : true},
           {data: 'action', name: 'action', orderable: false, searchable: false},
       ],

       responsive:true,
       order:[0,'desc']
   });      
        } else {
     var table = $('.reserv-table').DataTable({
       "language": {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json"},
       processing: true,
       serverSide: true,
       destroy: true,
       ajax: {
       "url": "{{ route('allreserve') }}",
       "data": {
           "start_date": start,
           "end_date": end,
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
          }
        },
       columns: [
           {data: 'id', name: 'id'},
           {data: 'title', name: 'title',searchable: true, sortable : true},
           {data: 'from', name: 'from',searchable: true, sortable : true},
           {data: 'to', name: 'to',searchable: true, sortable : true},
           {data: 'price', name: 'price',searchable: true, sortable : true},
           {data: 'paid', name: 'paid',searchable: true, sortable : true},
           {data: 'remain', name: 'remain',searchable: true, sortable : true},
           {data: 'user', name: 'user',searchable: true, sortable : true},
           {data: 'action', name: 'action', orderable: false, searchable: false},
       ],

       responsive:true,
       order:[0,'desc']
   }); 

}
            $('#ModalShow').modal('show');
            $('body').on('click', '#add_resrve', function () {
                $('#ModaladdResrve').modal('show');
            });
        },

        events: [
        @foreach($events as $event)
            {
                title: '{{$event->entity->with('detailes')->first()->title}}',
                start: '{{$event->from}}',
                end: '{{$event->to}}T24:00:00',
                display: 'background',
                color:'@if($event->created == 'admin'){{'#419af5'}};@elseif($event->created == 'user'){{'#ad1f1c'}};@else{{'#29612d'}};@endif'       
          },
          @endforeach
        ]
    });
    
</script>

@endpush