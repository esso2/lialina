@extends('admin.layout.master')

@section('content')

<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.roles')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.roles')</a></li>
                            <li class="active">@lang('admin.addroles')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <form action="{{route('roles.store')}}" method="post" data-parsley-validate>
                                @csrf
                                @method('post')
                          
                                <div class="form-group col-md-6">
                                    <label for="name">@lang('admin.namerole')</label>
                                    <input type="text" name="name"
                                     value="{{old('name')}}" 
                                     required parsley-trigger="change" 
                                     placeholder="@lang('admin.namerole')" 
                                     class="form-control" >
                                </div>
                                <div class="form-group">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>@lang('admin.model')</th>
                                            <th>@lang('admin.permissions')</th>
                                        </tr>
                                        @foreach($models as $index=>$model)
                                        @if($model == 'settings')
                                        <?php $maps = ['read', 'update']; ?>
                                        @endif
                                        @if($model == 'contactus')
                                        <?php $maps = ['read', 'delete']; ?>
                                        @endif
                                            <tr>
                                                <td style="width:5%">{{$index+1}}</td>
                                                <td style="width:5%">@lang('admin.'.$model)</td>
                                                <td>
                                                    <select class="form-control select2me" name="permissions[]" multiple>
                                                        @foreach($maps as $index=>$map)
                                                            <option value="{{$model.'-'.$map}}">@lang('admin.'.$map)</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="clearfix"></div>
                                <div class="form-group text-right m-b-0">
                                    @if(Auth::guard('admin')->user()->hasPermission('roles-create'))
                                    <button class="btn btn-primary waves-effect waves-light" type="submit" name="submit">@lang('admin.add')</button>
                                    @else
                                    <button class="btn btn-primary waves-effect waves-light" disabled>@lang('admin.add')</button>
                                    @endif
                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">@lang('admin.cancel')</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection