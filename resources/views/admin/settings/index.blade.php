@extends('admin.layout.master')

 @section('content')

 <div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.settings')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.settings')</a></li>
                            <li class="active">@lang('admin.updatesetting')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
             <form method="POST" id="setting" enctype="multipart/form-data" data-parsley-validate>
                @csrf
                @method('post')
                <div class="form-group col-md-6">
                    <label for="title">@lang('admin.webtitle')</label>
                    <input type="text" name="title" value="{{$setting->title}}" required parsley-trigger="change" placeholder="@lang('admin.title')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="phone">@lang('admin.phone')</label>
                    <input type="text" name="phone"  value="{{$setting->phone}}" parsley-trigger="change" required placeholder="@lang('admin.phone')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="whatsapp">@lang('admin.whatsapp')</label>
                    <input type="text" name="whatsapp"  value="{{$setting->whatsapp}}" parsley-trigger="change" required placeholder="@lang('admin.whatsapp')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="facebook">@lang('admin.facebook')</label>
                    <input type="text" name="facebook" value="{{$setting->facebook}}"  required parsley-trigger="change" placeholder="@lang('admin.facebook')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="instagram">@lang('admin.instagram')</label>
                    <input type="text" name="instagram" value="{{$setting->instagram}}"  parsley-trigger="change" required placeholder="@lang('admin.instagram')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="twitter">@lang('admin.twitter')</label>
                    <input type="text" name="twitter" value="{{$setting->twitter}}"  parsley-trigger="change" required placeholder="@lang('admin.twitter')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="linkedin">@lang('admin.linkedin')</label>
                    <input type="text" name="linkedin" value="{{$setting->linkedin}}"  parsley-trigger="change" required placeholder="@lang('admin.linkedin')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="email">@lang('admin.email')</label>
                    <input type="text" name="email" value="{{$setting->email}}"  parsley-trigger="change" required placeholder="@lang('admin.email')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="website">@lang('admin.website')</label>
                    <input type="text" name="website" value="{{$setting->website}}"  parsley-trigger="change" required placeholder="@lang('admin.website')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="address">@lang('admin.addressar')</label>
                    <input type="text" name="address" value="{{$setting->address}}"  parsley-trigger="change" required placeholder="@lang('admin.address')" class="form-control" >
                </div>

                <div class="form-group col-md-6">
                    <label for="logo">@lang('admin.logo')</label>
                    <input type="file" name="logo" id="logo" class="filestyle image" required data-buttonname="btn-primary">
                </div>

                <div class="form-group col-md-6">
        <img src="{{$setting->image_path}}" width="150" height="100" alt="" class="image-show"style="margin-right: 130px;"/>
                </div>
                            
                     <div class="clearfix"></div>
            <div class="form-group text-right m-b-0">
                @if(auth()->user()->hasPermission('settings-update'))
                <button class="btn btn-primary waves-effect waves-light" type="submit" name="submit">@lang('admin.add')</button>
                @else
                <button class="btn btn-primary waves-effect waves-light" disabled>@lang('admin.add')</button>
                @endif
                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">@lang('admin.cancel')</button>
            </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
 @endsection

 @push('js')

 <script>
    $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#setting',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                $.ajax({
                    url: '{{ route('updatesetting')}}',
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.updatesuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },

                });
            });

        });
 </script>

 @endpush