@extends('admin.layout.master')

@section('content')

<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.terms')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.terms')</a></li>
                            <li class="active">@lang('admin.addterms')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">

                <form id="terms" method="post" data-parsley-validate>
                @csrf
                @method('post')
                
            @foreach (config('translatable.locales') as $locale)
                <div class="form-group col-md-6">
                    <label for="title">@lang('admin.' .$locale. '.termtitle')</label>
                    <input type="text" name="{{$locale}}[title]"
                        value="{{old($locale . '.title')}}" 
                        required parsley-trigger="change" 
                        placeholder="@lang('admin.'.$locale .'.termtitle')" 
                        class="form-control" >
                </div>
            @endforeach
                

                @foreach (config('translatable.locales') as $locale)
                <div class="form-group col-md-6">
                    <label for="description">@lang('admin.' .$locale. '.description')</label>
                    <textarea type="text" name="{{$locale}}[description]"
                        value="{{old($locale . '.description')}}" 
                        required parsley-trigger="change" 
                        placeholder="@lang('admin.'.$locale .'.description')" 
                        class="form-control" ></textarea>
                </div>
                @endforeach
               
                <div class="clearfix"></div>

                <div class="form-group text-right m-b-0">
                    @if(Auth::guard('admin')->user()->hasPermission('terms-create'))
                    <button class="btn btn-primary waves-effect waves-light" type="submit" name="submit">@lang('admin.add')</button>
                    @else
                    <button class="btn btn-primary waves-effect waves-light" disabled>@lang('admin.add')</button>
                    @endif
                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">@lang('admin.cancel')</button>
                </div>

                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#terms',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                $.ajax({
                    url: '{{ route('terms.store')}}',
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.addsuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },

                });
            });

        });
 </script>
@endpush