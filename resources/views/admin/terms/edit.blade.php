@extends('admin.layout.master')

@section('content')

<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.terms')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.terms')</a></li>
                            <li class="active">@lang('admin.editterms')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <form id="terms" method="post" data-parsley-validate>
                                @csrf
                                @method('put')
                                <input type="hidden" name="termsId" id="terms_id" value="{{$term->id}}"  class="form-control">
                              
                                @foreach (config('translatable.locales') as $locale)
                                <div class="form-group col-md-6">
                                    <label for="title">@lang('admin.' .$locale. '.title')</label>
                                    <input type="text" name="{{$locale}}[title]"
                                        value="{{$term->translate($locale)->title}}" 
                                        required parsley-trigger="change" 
                                        placeholder="@lang('admin.'.$locale .'.title')" 
                                        class="form-control" >
                                </div>
                            @endforeach
                                
                
                                @foreach (config('translatable.locales') as $locale)
                                <div class="form-group col-md-6">
                                    <label for="description">@lang('admin.' .$locale. '.description')</label>
                                    <textarea type="text" name="{{$locale}}[description]"
                                        value="" 
                                        required parsley-trigger="change" 
                                        placeholder="@lang('admin.'.$locale .'.description')" 
                                        class="form-control" >{{$term->translate($locale)->description}}</textarea>
                                </div>
                                @endforeach


                
                                <div class="clearfix"></div>
                                <div class="form-group text-right m-b-0">
                                    @if(Auth::guard('admin')->user()->hasPermission('terms-update'))
                                    <button class="btn btn-primary waves-effect waves-light" type="submit" name="submit">@lang('admin.edit')</button>
                                    @else
                                    <button class="btn btn-primary waves-effect waves-light" disabled>@lang('admin.edit')</button>
                                    @endif
                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">@lang('admin.cancel')</button>
                                </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#terms',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                 let id = $('#terms_id').val();
                 var url = '{{ route('terms.update', ':id') }}';
                  url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,
                    success: function (response) {
                        if (response.errors){
                            $.each(response.errors, function( index, value ) {
                                new Noty({
                                    type: 'success',
                                    layout: 'topRight',
                                    text: value,
                                    timeout: 2000,
                                    killer: true
                                }).show();
                            });
                        }
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.updatesuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },

                });
            });

        });
 </script>
@endpush