@extends('admin.layout.master')

@push('css')
<style>
.field-icon {
  float: left;
  margin-left: 15px;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}
</style>
@endpush

@section('content')

<div class="wrapper">
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">@lang('admin.users')</h4>
                        <ol class="breadcrumb">
                            <li><a href="">@lang('admin.users')</a></li>
                            <li class="active">@lang('admin.editusers')</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <form id="users" method="post" enctype="multipart/form-data" data-parsley-validate>
                                @csrf
                                @method('put')
                                <input type="hidden" name="UserId" id="user_id" value="{{$user->id}}"  class="form-control">
                              
                                <div class="form-group col-md-5">
                                </div>
                                <div class="form-group">
                                    <img src="{{$user->image_path}}" 
                                    width="130" height="130" alt="" 
                                    style="margin-right: 25px;" 
                                    class="image-show img-circle"/>
                                 </div>

                                <div class="form-group col-md-6">
                                    <label for="name">@lang('admin.name')</label>
                                    <input type="text" name="name"
                                        value="{{$user->name}}" 
                                        required parsley-trigger="change" 
                                        placeholder="@lang('admin.name')" 
                                        class="form-control" >
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="lastname">@lang('admin.lastname')</label>
                                    <input type="text" name="lastname"
                                        value="{{$user->lastname}}" 
                                        required parsley-trigger="change" 
                                        placeholder="@lang('admin.lastname')" 
                                        class="form-control" >
                                </div>
                
                                <div class="form-group col-md-6">
                                    <label for="email">@lang('admin.email')</label>
                                    <input type="email" name="email"
                                        value="{{$user->email}}" 
                                        required parsley-trigger="change" 
                                        placeholder="@lang('admin.email')" 
                                        class="form-control" >
                                </div>
                
                                <div class="form-group col-md-6">
                                    <label for="password">@lang('admin.password')</label>
                                    <input type="password" name="password"
                                        value="{{old('password')}}" 
                                        placeholder="@lang('admin.password')" 
                                        class="form-control" id="password-field">
                                        <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                </div>

                                <div class="form-group col-md-3">
                                </div>
                                <div class="form-group col-md-6" style="text-align: center;margin-top: 17px;">
                                    <label for="image"> @lang('admin.imageprofile')</label>
                                    <input type="file" name="image" id="image" class="filestyle image" data-buttonname="btn-primary">
                                </div>
                

                
                                <div class="clearfix"></div>
                                <div class="form-group text-right m-b-0">
                                    @if(Auth::guard('admin')->user()->hasPermission('users-update'))
                                    <button class="btn btn-primary waves-effect waves-light" type="submit" name="submit">@lang('admin.edit')</button>
                                    @else
                                    <button class="btn btn-primary waves-effect waves-light" disabled>@lang('admin.edit')</button>
                                    @endif
                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">@lang('admin.cancel')</button>
                                </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

<script>
        $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                input.attr("type", "text");
                } else {
                input.attr("type", "password");
                }
        });
</script>

<script>
    $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('body').on('submit','#users',function (e) {
                e.preventDefault();
                 //alert('asdasd');
                 let id = $('#user_id').val();
                 var url = '{{ route('users.update', ':id') }}';
                  url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    method: "post",
                    data: new FormData(this),
                    dataType: 'json',
                    cache       : false,
                    contentType : false,
                    processData : false,

                    success: function (response) {
                        //console.log(response);
                        if (response.errors){
                            $.each(response.errors, function( index, value ) {

                                // console.log(value);

                                new Noty({
                                    type: 'success',
                                    layout: 'topRight',
                                    text: value,
                                    timeout: 2000,
                                    killer: true
                                }).show();
                            });
                        }
                        if(response.status == 'success'){
                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: "@lang('admin.updatesuccessfully')",
                                timeout: 5000,
                                killer: true
                            }).show();
                        }
                    },

                });
            });

        });
 </script>
@endpush