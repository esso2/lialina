<?php

use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\AdminsController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\EntityController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\ReserveController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\TermController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\AdminController;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){ 
        
        Route::get('admin/home', [AdminController::class, 'index'])->name('adminhome');
        Route::GET('admin', [LoginController::class, 'showLoginForm'])->name('admin.login');
        Route::POST('admin',[LoginController::class, 'login']);
        Route::GET('logout',[LoginController::class, 'logout'])->name('admin.logout');

        Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin']], function () {

        Route::get('setting', [SettingController::class, 'index'])->name('setting');
        Route::post('setting', [SettingController::class, 'update'])->name('updatesetting');

        Route::resource('roles', RoleController::class)->except(['show']);

        Route::resource('terms', TermController::class)->except(['show']);
        
        Route::resource('admins', AdminsController::class)->except(['show']);
        Route::get('adminactive', [AdminsController::class, 'AdminStatus'])->name('adminactive');

        Route::resource('users', UsersController::class)->except(['show']);
        Route::get('useractive', [UsersController::class, 'UserStatus'])->name('useractive');

        Route::resource('contactus', ContactController::class)->except(['create','edit']);
        Route::resource('aboutus', AboutController::class)->except(['show']);

        Route::resource('categories', CategoryController::class)->except(['show']);
        Route::get('categoryactive', [CategoryController::class, 'CategoryStatus'])->name('categoryactive');

        Route::resource('regions', RegionController::class)->except(['show']);
        Route::get('regionactive', [RegionController::class, 'RegionStatus'])->name('regionactive');

        Route::resource('entities', EntityController::class);
        Route::put('entityupdate2/{id}', [EntityController::class, 'updateStep2'])->name('updatestep2');
        Route::get('entityactive', [EntityController::class, 'EntityStatus'])->name('entityactive');
        Route::post('entityimagestore',[EntityController::class, 'EntityImage'])->name('entityimg');
        Route::get('entityimageshow',[EntityController::class, 'ShowImage'])->name('entityimgshow');
        Route::post('entityimagedelete',[EntityController::class, 'EntityImageDelete'])->name('entityimgdelete');

        Route::post('entityfeatadd',[EntityController::class, 'EntityFeatureAdd'])->name('entityfeatadd');
        Route::get('entityfeatall',[EntityController::class, 'EntityFeatureAll'])->name('entityfeatall');
        Route::delete('entityfeatdelete/{id}',[EntityController::class, 'EntityFeatureDelete'])->name('featureDelete');

        Route::post('entityfiltadd',[EntityController::class, 'EntityFilterAdd'])->name('entityfiltadd');
        Route::delete('entityfiltdelete/{id}',[EntityController::class, 'EntityFilterDelete'])->name('filterDelete');
        Route::delete('entityfiltoptiondelete/{id}',[EntityController::class, 'EntityOptionDelete'])->name('optionDelete');
        Route::post('entityoptiadd',[EntityController::class, 'EntityOptionAdd'])->name('entityoptionadd');

        Route::resource('reserves', ReserveController::class);
        Route::get('allres',[ReserveController::class, 'GetAllReseve'])->name('allreserve');
        
        });

        
    });


